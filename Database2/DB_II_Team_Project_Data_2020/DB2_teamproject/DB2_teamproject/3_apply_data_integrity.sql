/* Table No. 1 - Adult */
-- between Adult table and members 
alter table Adult
	add constraint fk_Adult_Members foreign key (memID)
	references Members (memID)
;

-- between Adult and Employees
alter table Adult
	add constraint fk_Adult_Employees foreign key (empID)
	references Employees(empID)
;

/* Table No. 2 - Authors */
-- Only primary key

/* Table No. 3 - Cards */
-- between Cards and Members
alter table Cards
	add constraint fk_Cards_Members foreign key (memID)
	references Members(memID)
;

-- between Cards and CardStatus
alter table Cards
	add constraint fk_Cards_CardStatus foreign key (statusID)
	references CardStatus(statusID)
;

/* Table No. 4 - CardStatus */
-- Lookup table

/* Table No. 5 - Employees */
-- Only primary key

/* Table No. 6 - Items */
-- between Items and Languages
alter table Items
	add constraint fk_Items_Languages foreign key (langID)
	references Languages (langID)
;

-- between Items and Authors
alter table Items
	add constraint fk_Items_Authors foreign key (authorID)
	references Authors (authorID)
;

/* Table No. 7 - Languages */
-- Lookup table

/* Table No. 8 - Loans */
-- between Loans and Items
alter table Loans
	add constraint fk_Loans_Items foreign key (ISBN)
	references Items (ISBN)
;

-- between Loans and Members
alter table Loans
	add constraint fk_Loans_Members foreign key (memID)
	references Members (memID)
;

/* Table No. 9 - Members */
-- between Members and Members
alter table Members
	add constraint fk_Members_Members foreign key (adultMemID)
	references Members (memID)
;

/* Table No. 10 - WaitingList */
-- between WaitingList and Items
alter table WaitingList
	add constraint fk_WaitingList_Items foreign key (ISBN)
	references Items (ISBN)
;

-- between WaitingList and Members
alter table WaitingList
	add constraint fk_WaitingList_Members foreign key (memID)
	references Members (memID)
;


/** Modify the following tables **/
/* 1. Create or modify the default constraint that makes WA (Washington) 
the default for the state column in the adult table. */
alter table Adult
	add constraint df_state_Adult default ('WA') for state
;
/* 2. Create a phone number constraint to the adult table. */
 
/* 3. Write and execute a statement that adds a constraint to the due_date 
column in the loan table. The value in the due_date column must be greater than 
or equal to the value in the out_date column (due_date >= out_date). */
alter table Loans
	add constraint ck_dateDue_dateOut_Loans
	check (dateDue >= dateOut)
;