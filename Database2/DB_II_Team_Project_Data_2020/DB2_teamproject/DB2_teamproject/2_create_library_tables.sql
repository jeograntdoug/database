use Library;

-- create Items table
create table Items
(
	ISBN varchar(13) not null primary key,
	title nvarchar(30) not null,
	langID char(2) not null,	-- foreign key Languages(langID)
	hardCover bit not null default (0),
	synopsis nvarchar(255) not null,
	authorID smallint not null	-- foreign key Authors(authorID)
)
;

-- create Members table
create table Members
(
	memID smallint not null primary key,
	memFN nvarchar(20) not null,
	mInitial nvarchar(5) null,
	memLN nvarchar(20) not null,
	dateBirth date not null,
	photo varbinary(max) null,
	adultMemID smallInt null	-- foreign key Members(memID)
)
;

-- create Adult table
create table Adult
(
	memID smallint not null primary key,	-- foreign key Members(memID)
	street nvarchar(40) not null,
	city nvarchar(15) not null,
	state char(2) not null,
	zip varchar(10) not null,
	phone varchar(15) not null,
	dateExp date not null,
	empID smallint null		-- foreign key Employees(empID)
)
;

-- create Employees table
create table Employees
(
	empID smallint not null primary key,
	empFN nvarchar(20) not null,
	empInitial nvarchar(5) null,
	empLN nvarchar(20) not null,
	dataBirth date not null,
	street nvarchar(40) not null,
	city nvarchar(15) not null,
	state char(2) not null,
	zip varchar(10) not null,
	phone varchar(15) not null
)
;

-- create Languages table
create table Language
(
	langID char(2) not null primary key,
	description nvarchar(20) not null
)
;
execute sp_rename 'Language','Languages';

-- create WaitingList table
create table WaitingList
(
	WListID int not null primary key,
	ISBN varchar(13) not null, -- foreign key Items(ISBN)
	memID smallint not null, -- foreign key Members(memID)
)
;
alter table WaitingList
	add created_at date not null
;

-- create Loans table
create table Loans
(
	LoanID int not null primary key,
	ISBN varchar(13) not null,	-- foreign key Items(ISBN)
	memID smallint not null,	-- foreign key Members(memID)
	dateOut date not null,
	dateDue date not null,
	dateIn date not null
)
;

-- create Cards table
create table Cards
(
	cardID smallint not null primary key,
	memID smallint not null,	-- foreign key Members(memID)
	statusID char(1) not null,	-- foreign key CardStatus(statusID)
)
;

-- create CardStatus table
create table CardStatus
(
	statusID char(1) not null primary key,
	description nvarchar(15) not null
)
;

--create Authors table
create table Authors
(
	authorID smallint not null primary key,
	authorFN nvarchar(20) not null,
	autInitial nvarchar(5) null,
	authorLN nvarchar(20) not null,
	dateBirth date null,
	country nvarchar(20) null
)
;