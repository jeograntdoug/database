/*
	Purpose: create library database
	Script date: January 27,2020
	Developed By:
*/

use master
;
go

create database Library
on primary
(
	Name = 'Library',
	size = 12MB,
	filegrowth = 4MB,
	filename = 'c:\Program files\microsoft sql server\mssql14.mssqlserver\mssql\data\Library.mdf'
)
log on
(
	Name = 'Library_log',
	size = 3 MB,
	filegrowth = 10%,
	filename = 'c:\Program files\microsoft sql server\mssql14.mssqlserver\mssql\data\Library_log.ldf'
)
;