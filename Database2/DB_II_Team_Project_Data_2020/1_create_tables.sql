CREATE DATABASE	Library;

use Library;

CREATE TABLE Items
(
	ISBN varchar(13) not null primary key,
    title nvarchar(30) not null,
    langID tinyint not null,	-- foreign key Language(langID)
    hardCover boolean not null,
    synopsis nvarchar(255) null,
    authorID smallint not null	-- foreign key Authors(authorID)
)
;

CREATE TABLE Members
(
	memID smallint not null primary key,
    memFN nvarchar(20) not null,
    mInitial nvarchar(5) null,
    memLN nvarchar(20) not null,
    dateBirth date not null,
    photo blob null,
    adultMemID smallint null -- foreign key Members(memID)
)
;

CREATE TABLE Adult
(
	memID smallint not null primary key,	-- foreign key Mombers(memID)
    street nvarchar(40) not null,
    city nvarchar(15) not null,
    state char(2) not null,
    zip varchar(10) not null,
    phone varchar(15) null,
    dateExp date not null,
    empID smallint null	-- foreign key Employees(empID)
)
;
/** WIP
CREATE TABLE Employees
(
	empID smallint not null primary key,
    empFN nvarchar(20) not null,
    empInitial nvarchar(5) null,
    empLN nvarchar(20) not null,
)
;
*/
