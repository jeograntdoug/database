/*	Purpose: Transfer and Rename Objects
	Script Date: January 24, 2020
*/

use HSafeway
;

/* Transfer a table from one schema to another
Syntax:
alter schema to_schema_name transfer from_schema_name.table_name */

alter schema Sales transfer Production.Shippers
;
execute sp_rename 'Sales.CustomerGeographics','CustomerDemographics';

create database textDB;
use textDB;
create table textDB
(
	ID smallint identity not null primary key
)
;

/* rename database objects: database, table, column
1. rename a database
	execute sp_renamedb 'old_database_name','new_database_name'

2. rename a table
	execute sp_rename 'schema_name.old_table_name', 'new_table_name'

3. rename column
	execute sp_rename 'schema_name.table_name.old_column_name', new_olumn_name', 'column'

*/

execute sp_renamedb 'textDB' , 'testDB';
execute sp_rename 'dbo.textDB','tbltest';
execute sp_rename 'dbo.tbltest.ID','testID','column';
SELECT * from tbltest;

use HSafeway
;
drop database testDB;

/* change the column definition 
1. in Access and SQL Server
	alter table table_name
		alter column column_name data_type constraint

2. in MySql
	alter table table_name
		modify column column_name data_type constraint

3. in Oracle
	alter table table_name
		modify column_name data_type constraint
*/

/* change the UnitPrice data type from money to decimal(6,2) */
alter table Production.Products
	alter column UnitPrice decimal(6,2) not null
;

/* change picture to varbinary in Categories */
alter table Production.Categories
	alter column Picture varbinary(max) null
;