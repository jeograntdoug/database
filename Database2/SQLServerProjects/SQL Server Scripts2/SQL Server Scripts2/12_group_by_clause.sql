use Safeway20H1;

select SC.Country, SC.Region, SC.City, count(SO.orderID) as TotalOrders
from Sales.Customers as SC
	inner join Sales.Orders as SO
		on SC.CustomerID = SO.CustomerID
group by SC.Country, SC.Region, SC.City
;

select SC.Country, SC.Region, SC.City, count(SO.orderID) as TotalOrders
from Sales.Customers as SC
	inner join Sales.Orders as SO
		on SC.CustomerID = SO.CustomerID
group by cube (SC.Country, SC.Region, SC.City)
;

select productid, orderid, quantity
from sales.[order details];

select productid, sum(quantity) as total_quantity
from Sales.[Order Details]
group by productid
;

select productid, Count(quantity) as total_quantity
from Sales.[Order Details]
group by productid
having Count(quantity) >= 30
order by count(quantity) desc
;

select productid, orderid, SUM(quantity) as total_quantity
from Sales.[Order Details]
group by productid, orderid
with rollup
order by productid, orderid
;

select productid, orderid, SUM(quantity) as total_quantity
from Sales.[Order Details]
group by productid, orderid
with cube
order by productid, orderid
;

select productid, grouping(productid), orderid, grouping(orderid),
sum(quantity) as total_quantity
from Sales.[Order Details]
group by productid, orderid
with cube
order by productid, orderid
;