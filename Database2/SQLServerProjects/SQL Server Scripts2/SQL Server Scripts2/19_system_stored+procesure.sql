/*
	Purpose:
*/

use Safeway20H1;
go


/* return the list of all databases in SQL Server */
execute sp_databases;
go

select * from sys.databases;
go

execute sp_datatype_info;
go

/* return inforamtion about [Safeway20H1] database */
exec sp_helpdb 'Safeway20H1';
go

/* return information about specific object in a database */
execute sp_help @objname = 'Sales.Customers';
go

/* return all tables in a specific schema */
execute sp_tables
--@table_name = 'Orders',
@table_owner = 'Sales';
go

/* return constraints applyed to the Slaes.Orders table */
execute sp_helpconstraint 'Sales.Orders'
;
go

/*
1. write a query that retreive system metadata
2. using system catalog(database) views
3. using standard information_schema views
4. using system functions
*/

-- 1. write a query that retreive system metadata
/* view the server settings using system views and functions */
select name, value, value_in_use, description
from sys.configurations
;
go

/* return the list of all databases in SQL Server */
select *
from sys.databases;

select name, database_id, collation_name, user_access, user_access_desc, state_desc
from sys.databases
;
go

/* list all user-defined tables */
select 
	S.name as 'Schema Name',
	T.name as 'Table Name',
	T.type_desc as 'Table Description',
	T.create_date as 'Created Date'
from sys.tables as T
	join sys.schemas as S
		on T.schema_id = S.schema_id
order by S.name, T.name
;


-- 2. using system catalog views
select *
from information_schema.tables
where table_type = 'base table'
;
go

select *
from information_schema.views
;
go

select *
from information_schema.view_column_usage
where view_schema = 'Sales'
and view_name = 'OrderTotalView'
;
go

/* return the list of all stored procedures */
exec sp_stored_procedures
;
go


-- 4. using system functions

/* return the list of all user-defined functions in a databse */
select 
	name as 'Function Name',
	SCHEMA_NAME(schema_id) as 'Schema Name',
	type_desc as 'Description',
	create_date as 'Created Date',
	modify_date as 'Modified Date'
from sys.objects
where type_desc like '%function%'
;
go

