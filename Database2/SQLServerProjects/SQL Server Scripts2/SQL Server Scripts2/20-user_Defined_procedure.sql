/*create user defined procedure
date:january 30,2020
BY:Maha M. Shawkat
*/
/*create a procedure,humanResources.getAllEmployeeSP, that that returns the employee first name,last name, and title
*/
create procedure humanResources.getAllEmployeeSP
as 
begin
select firstName,lastName,title
from humanResources.employees
end
;
go
/*call the procedure*/
exec humanResources.getAllEmployeeSP
;
go
/* create a proceure humanResources.getAllEmployeeByNameSP , that returns the amployee firstName,lastName, and title having (knowing) the employee number*/
create procedure humanResources.getAllEmployeeByNameSP 
(--declare the parameters
--@parameterName as dta_type [=expression]
@employeeID as int
)
as
begin
select firstName,lastName,title
from humanResources.employees
where employeeID=1--@employeeID
end
;
go
/*call the procedure*/
exec humanResources.getAllEmployeeByNameSP @employeeID=1
;
go
/*Create a procedure humanResources.getAllEmployeeBytitleSP, that returns the employee full name, phone and title, knowing the job title*/
alter procedure humanResources.getAllEmployeeBytitleSP
(
@title as nvarchar(30)
)
as 
begin
	select concat_ws(' ',firstName,lastName)as 'full name',
				homephone, 
				title
	from HumanResources.Employees 
	where title like @title
	end 
;
go
/*call the procedure*/
exec humanResources.getAllEmployeeBytitleSP @title='sales rep%'
;
go
/*create a aprocedure ,production.getProductListSP,that return a list on products with prices that dont exceeds a specific amount*/
alter procedure production.getProductListSP
(
--declare the prameters
@productName as nvarchar(20),
@maxPrice as money, --product maximum price
@comparePrice as money output,
@ListPrice as money out
)
as 
begin
select pp.productName as 'product name', sod.UnitPrice as 'selling price'
from production.Products as pp
inner join sales.[order details] as SOD
on pp.ProductID=SOD.ProductID
where pp.productName like @productName 
and SOD.unitPrice<@maxPrice
--populate the output parameters
set @ListPrice =
(
select max(SOD.unitPrice) as 'maximum price'
from production.Products as pp
inner join sales.[order details] as SOD
on pp.ProductID=SOD.ProductID
where sod.unitPrice< 50 --@maxPrice
)
--set the @comparePrice=@maxPrice
 set @comparePrice=@maxPrice
end
;
go
/*testing the procedure*/
declare @comparePrice as money, @sellingPrice as money
execute production.getProductListSP '%Grandma%',30,@comparePrice output, @sellingPrice output
if( @sellingPrice<=@comparePrice)
begin
print 'These products can be purchased for less than $'+rtrim(cast(@comparePrice as nvarchar(20)))+'.'
end
else 
begin 
print 'These prices in this product exceed $'+rtrim(cast(@comparePrice as nvarchar(20)))+'.'
end

; 
go
select*
from production.products
where productName like '%mishi%';
;
go
