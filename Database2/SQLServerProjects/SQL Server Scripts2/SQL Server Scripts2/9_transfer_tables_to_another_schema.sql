use Safeway20H1
;
go
/* a schema is a distinct namespace to facilitate the separation, organization, management, and ownership of database objects.
SYNTAX:
create schema object_name authorization owner_name
*/

-- create person schema
/* Check owner_name
select
	USER_NAME() as 'User Name',
	DB_NAME() as 'Database Name',
	@@SERVERNAME as 'Server Name'
;
*/
select 
	User_NAME() as 'User Name',
	DB_NAME() as 'Database Name',
	@@SERVERNAME as 'Server Name'
;

execute sp_helpdb
;



create schema Person authorization dbo
;
go
-- 2) create HumanResources schema
create schema HumanResources authorization dbo
;
go
-- 3) create Sales schema
create schema Sales authorization dbo
;
go
-- 4) create Productions schema
create schema Production authorization dbo
;
go

/* Syntax to transfer a table from one schema to another
alter schema to_schema_name TRANSFER from_schema_name.table_name
*/
/***** Table No. 1 - Sales.Customers ****/
alter schema Sales transfer dbo.Customers;

/***** Table No. 2 - Sales.Order ****/ 
alter schema Sales transfer dbo.Orders;
/***** Table No. 3 - Production.Products ****/ 
alter schema Production transfer dbo.Products;
/***** Table No. 4 - Sales.[Order Details] ****/ 
alter schema Sales transfer dbo.[Order Details];
/***** Table No. 5 - Production.Suppliers ****/
alter schema Production transfer dbo.Suppliers;
/***** Table No. 6 - Production.Categories ****/ 
alter schema Production transfer dbo.Categories;
/***** Table No. 7 - Production.Shippers ****/
alter schema Production transfer dbo.Shippers;
/***** Table No. 8 - HumanResources.Employees ****/
alter schema HumanResources transfer dbo.Employees;
/***** Table No. 9 - HumanResources.Region ****/
alter schema HumanResources transfer dbo.Region;
/***** Table No. 10 - HumanResources.Territories ****/
alter schema HumanResources transfer dbo.Territories;
/***** Table No. 11 - HumanResources.EmployeeTerritories ****/
alter schema HumanResources transfer dbo.EmployeeTerritories;
/***** Table No. 12 - Sales.CustomerCustomerDemo ****/
alter schema Sales transfer dbo.CustomerCustomerDemo;
/***** Table No. 13 - Sales.CustomerDemographics ****/
alter schema Sales transfer dbo.CustomerDemographics;
