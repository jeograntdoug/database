/*
	Built-In T-SQL Functions
	Script Date: January 30, 2020
	Developed by: 
*/

/* add a statement that specifies the script runs in the context of the master database */

use Safeway20H1
;
go

-- some of the T-SQL date functions
select OrderID as 'Order',
	OrderDate as 'Order Date',
	YEAR(OrderDate) as 'Order Year',
	MONTH(OrderDate) as 'Order Month',
	DAY(OrderDate) as 'Order Day',
	DATEPART(year,OrderDate) as 'Year Date Part',
	DATEPART(month,OrderDate) as 'Month Date Part',
	DATEPART(day, OrderDate) as 'Day Date Part',
	DatePart(weekday, OrderDate) as 'Weekday Date Part',
	DatePart(DayOfYear, OrderDate) as 'Day of Year Date Part',
	DateName(weekday,OrderDate) as 'Day name',
	EOMONTH(OrderDate) as 'End of Month'
from Sales.Orders
;

/* return all orders placed between 2019 and 2020*/

select *
from HumanResources.Employees
where ISNUMERIC(Postalcode) = 1
;

/* IIF(expression) - Immediate If function that returns one or two values depending on whether the boolean expression eveluates to true or false
SYNTAX: IIF( (expression), 'true_value', 'false_value')
iif( (gender = 'f'), 'Female', 'Male')
*/

/* return the value 'Low Price' if the product unit price is less than $50, otherwise - return 'Hight Price' */
select ProductName, UnitPrice, iif( (UnitPrice < 50), 'Low Price','High Price') as 'Price'
from Production.Products
;

/* CASE espression evaluates a list of items and returns one of multiple result expression */
select
	ProductName,
	UnitPrice,
	CASE
		WHEN(UnitPrice < 50) then 'Low Price'
		WHEN(UnitPrice < 70) then 'Not Bad Price'
		WHEN(UnitPrice < 100) then 'Kinda expensive'
		ELSE 'Too Expensive'
	END
from Production.Products
;

/* Choose function returns values at the specified index from a list of values
SYNTAX: choose(index, value1, value2, ...) where index is one-based
*/

select EmployeeID,choose(EmployeeID, 'Manager', 'Developer', 'Programmer','Analyst', 'Tester')
from HumanResources.Employees
order by EmployeeID
;

select FirstName, left(firstName,3) as 'Fir', lastName, right(lastName,3) as 'ame'
from HumanResources.Employees
;

select SYSDATETIME() as 'SYstem data and Time'
;
select DATALENGTH(CAST(SYSDATETIME() as varchar )) as 'varchar length',
	DATALENGTH(SYSDATETIME()) as 'Date length'
;
select (CONVERT(varchar, current_timestamp, 103)) as 'Britich/French Style'
;

select
PARSE('04/04/2020' as datetime using 'en-us') as 'US Date Style',
TRY_PARSE('10/13/2020' as datetime using 'fr-fr') as 'French Date Style'
;

select @@LANGID as 'Language ID'
;

set language 'arabic'
;

select @@LANGID as 'Language ID'
;

set language 'Korean'
;
declare @today as datetime
;
set @today = getdate()
;
select datename(month,@today) as 'Month Name'
;

execute sp_helplanguage 'Korean';

execute sp_help 'Sales.Orders'
;

set language 'English';

/* concatenate a string to an order number */
select isNull(N'SO' + convert(nvarchar(8), OrderID), N'*** Error ***') as 'Sales Order Number'
from Sales.Orders
;
