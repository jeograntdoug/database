/* Creating Triggers
Script Date: January 29, 2020
Developed by: Maha M.Shawkat
*/

use Safeway20h1
;
go 
/*create a trigger,notifyCustomeTR, that displays a message when anyone modifies or inserts data into the table sales.customers
*/
/*Syntax 
CREATE [ OR ALTER ] TRIGGER [ schema_name . ]trigger_name   
ON { table | view }   
[ WITH <dml_trigger_option> [ ,...n ] ]  
{ FOR | AFTER | INSTEAD OF }   
{ [ INSERT ] [ , ] [ UPDATE ] [ , ] [ DELETE ] }   
[ WITH APPEND ]  
[ NOT FOR REPLICATION ]   
AS { sql_statement  [ ; ] [ ,...n ] | EXTERNAL NAME <method specifier [ ; ] > }  
*/
create trigger sales.notifyCustomerChangesTR 
on sales.customers 
after insert,update
as
	raiserror('customer Table was modified',
	10, --user severity
	1--state
	)
	;

go
/*testing the trigger*/
select*
from sales.customers
; 
go 
/*change the contact name for customer id 'ALFKI' to 'Margret Drump'*/
update sales.customers
set contactName= 'Margret Drump'
where CustomerID= 'ALFKI'
;
go
/*creat a trigger ,HumanResources.checkModifiedDateTR, that checks the modified date column value.
this trigger ensures that during the insert of a new department, or updating an existing deparment, the modified date is the current date.if it is not  the row will be updated and set to the current date an time */

--create the deprtment table
create table  HumanResources.Departments
(
	DepartmentID int identity(1,1) not null,
	departmentName nvarchar(40) not null,
	modifiedDate dateTime null,
	constraint pk_departments primary key clustered( departmentID asc)
	)
;
go
--insert a new department
insert into HumanResources.Departments(departmentName)
values('techNet')
;
go
select *
from HumanResources.Departments
;
go
--create the trigger
create trigger HumanResources.checkModifiedDateTR
on HumanResources.Departments
for insert,update
as
	begin
	--declare variables
	declare @modifiedDate as dateTime,
			@DepartmentID as int
	--compute the return value
	select @modifiedDate=modifiedDate,
		   @departmentID=departmentID
	from inserted
	--making decision(comparing the modified date with the current date)
	if (abs(DATEDIFF(day,@modifiedDate,getDate()))>0) or (@modifiedDate is null)
		begin
			--set the modified date to the current date
			update HumanResources.Departments
			set modifiedDate =GETDATE()
			where DepartmentID=@departmentID
		end
	end

	/*change the name of department 1 to IT*/
	update HumanResources.Departments

	set departmentName='IT'
	where DepartmentID=1
	;
	go
	/* add a new department (in the passed date)*/
	insert into HumanResources.Departments 
	values('HR','9/24/2019')
	;
	go
	/* add a new department (in the future date)*/
	insert into HumanResources.Departments 
	values('Sales','9/24/2029')
	;
	go
	/* add a new department (without modifying date)*/
	insert into HumanResources.Departments (departmentName)
	values('Shipng')
	;
	go
	/*enable or disable trigger
	SYNTAX
		disable trigge schema_name.Trigger_name
		on schema_name.table_name
		
		enable trigge schema_name.Trigger_name
		on schema_name.table_name
	*/
	disable trigger HumanResources.checkModifiedDateTR
on HumanResources.Departments
;
go
insert into HumanResources.Departments (departmentName)
	values('production')
	;
	go

	/*Create a trigger HumanResources.employee.getEmployeeDateChangdTR, that audits any changes applied to employee data table*/

	--Create employee data
	create table humanResources.employeeData
	(
		employeeID int identity(1,1) not null,
		bankAccountNumber nchar(10) not null,
		salary money not null,
		socialSecurityNumber nchar(11)not null,
		lastName nvarchar(30)not null,
		FirstName nvarchar(30)not null,
		managerID int not null,
		constraint pk_employeeDate primary key clustered (EmployeeID)
	)
	;
	go
	--Create the auditEmployeeData table
	create table humanResources.auditEmployeeData
	(
		AuditLogID uniqueIdentifier default newID() not null,
		logType nchar(3) not null,--new or old
		AuditemployeeID int identity(1,1) not null,
		AuditbankAccountNumber nchar(10) not null,
		Auditsalary money not null,
		AuditsocialSecurityNumber nchar(11)not null,
		/*Return the login name associated with the security identification number (SID)*/
		AuditUser sysname default suser_name(),--built in function returns the number of users (susers_name())
		auditModifiedDate  dateTime default getDate()
	)
	;
	go
	alter table 
--set the relationShip between these two tables humanResources.auditEmployeeData and humanResources.employeeData
alter table humanResources.auditEmployeeData
add constraint fk_auditEmployeeData_employeeData foreign key (AuditemployeeID)
references humanResources.employeeData (employeeID)
;
go
alter trigger humanResources.employeeDataChangdTR
on humanResources.employeeData
for update
as 
	begin
		--audit the employeeData old record
		insert into HumanResources.auditEmployeeData
		(
			logType,
			AuditemployeeID,
			AuditbankAccountNumber,
			Auditsalary,
			AuditsocialSecurityNumber
		)
		select  'old',
				del.employeeID,
				del.bankAccountNumber,
				del.salary,
				del.socialSecurityNumber
		from deleted as del 
	union
	select  'new',
				ins.employeeID,
				ins.bankAccountNumber,
				ins.salary,
				ins.socialSecurityNumber
		from inserted as ins
	end
	;
	go
select*
from HumanResources.employeeData
;
go
select*
from HumanResources.auditEmployeeData
;
go
/*add a new record into human rescources.employeeDate table*/
insert into HumanResources.employeeData
(
bankAccountNumber,
salary,
socialSecurityNumber,
lastName,
FirstName,
managerID
)
values ('s-1234566', 45000,'123 456 678','smith','joh',32)
;
go
--increase john's salary to $47500
update HumanResources.employeeData
set salary=47500
where employeeID =2
; 
go
--increase john's salary to $47500
update HumanResources.employeeData
set salary=75000
where employeeID = 1
; 
go





	 


