/*
 purpose :Using JSon and XML
Script Date:February 4,2020
Develoed by: Maha M. Shawkat
*/
/*Add statement that specified that the script runs in the context of the master dataBase
*/
--Switch the master dataBase
use mmSafeway
;
go 
/*return the list of orders placed by customers.list customerID,Company name with the order number*/
select  C.CustomerId, c.companyName,OrderID
from sales.customers as c
	inner join sales.orders as o
		on C.CustomerId=o.customerID
for xml auto
;
go
/*Using element and Root options*/
select  C.CustomerId, c.companyName,OrderID
from sales.customers as c
	inner join sales.orders as o
		on C.CustomerId=o.customerID
for xml auto, elements,root('orders')
;
go
/*Return employee info for xml*/
select employeeID,firstName,LastName,homePhone
from HumanResources.employees
where EmployeeID in (1,2,3)
 for xml auto, elements,root('employees')
 ;
 go
 /*Return employee info for JSON*/
select employeeID,firstName,LastName,homePhone
from HumanResources.employees
where EmployeeID in (1,2,3)
 for JSON path,root('employees')
 ;
 go