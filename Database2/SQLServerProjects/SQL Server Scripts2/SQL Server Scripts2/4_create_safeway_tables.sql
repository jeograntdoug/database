/*	Purpose: Create Safeway Tables
	Script Date:
*/

use HSafeway
;

-- Create Safeway Tables

/* Using ONLY during the development process, check if an object exists 
by verifying the Object ID. If the object exists, it will be deleted using 
the DROP clause. If the object does not exist, it will be created.
*/
-- 'U' means user defined table
if OBJECT_ID('Sales.Customers', 'U') is not null
	drop table Sales.Customers
;


/***** Table No. 1 - Sales.Customers ****/
create table Sales.Customers
(
	customerid nchar(5) not null,
	companyname nvarchar(40) not null,
	contactname nvarchar(30) not null,
	contacttitle nvarchar(30) null,
	address nvarchar(60) not null,
	city nvarchar(15) not null,
	region nvarchar(15) null,
	postalcode nvarchar(10) not null,
	country nvarchar(15) not null,
	phone nvarchar(24) not null,
	fax nvarchar(24) null,
	-- constraint constraint)name constraint_type
	constraint pk_Customers primary key clustered (customerid asc)
)
;


/***** Table No. 2 - Sales.Order ****/
create table Sales.Orders
(
	orderid int identity not null,
	customerid nchar(5) not null,
	employeeid int not null,
	orderdate datetime not null,
	requireddate datetime not null,
	shipdate datetime null,
	shipvia int not null,
	freight money not null,
	shipname nvarchar(40) null,
	shipaddress nvarchar(60) null,
	shipcity nvarchar(15) null,
	shippostalcode nvarchar(10) null,
	shipregion nvarchar(15) null,
	shipcountry nvarchar(25) null,
	constraint pk_Orders primary key clustered (orderid asc)
)
;

/***** Table No. 3 - Production.Products ****/ 
create table Production.Products
(
	productid int identity not null,
	productname nvarchar(40) not null,
	SupplierID int not null,
	CategoryID int not null,
	QuantityPerUnit nvarchar(20) null,
	UnitPrice money null,
	UnitsInStock smallint null,
	UnitsOnOrder smallint null,
	ReorderLevel smallint null,
	Discontinued bit not null,
	constraint pk_Products primary key clustered (productid asc)
)
;


/***** Table No. 4 - Sales.[Order Details] ****/ 
create table Sales.[Order Details]
(
	orderid int not null,
	productid int not null,
	unitprice money not null,
	quantity smallint not null,
	discount real not null DEFAULT 0,
	constraint pk_OrderDetails primary key clustered (orderid asc,productid asc)
)
;

/***** Table No. 5 - Production.Suppliers ****/
create table Production.Suppliers
(
	supplierid int identity not null,
	companyname nvarchar(40) not null,
	contactname nvarchar(30) not null,
	contacttitle nvarchar(30) not null,
	address nvarchar(60) not null,
	city nvarchar(15) not null,
	region nvarchar(15) null,
	postalcode nvarchar(10) not null,
	country nvarchar(15) not null,
	phone nvarchar(24) not null,
	fax nvarchar(24) null,
	homepage nvarchar(60) null,
	Email nvarchar(60) not null,
	constraint pk_Suppliers primary key clustered (supplierid asc)
)
;


/***** Table No. 6 - Production.Categories ****/ 
create table Production.Categories
(
	categoryid int identity not null,
	categoryname nvarchar(15) not null,
	description nvarchar(250) null,
	picture varbinary(max) null
	constraint pk_Categories primary key clustered (categoryid asc)
)
;

/***** Table No. 7 - Production.Shippers ****/
create table Production.Shippers
(
	shipperid int identity not null,
	companyname nvarchar(40) not null,
	phone nvarchar(24) null,
	constraint pk_Shippers primary key clustered (shipperid asc)
)
;

/***** Table No. 8 - HumanResources.Employees ****/
create table HumanResources.Employees
(
	employeeid int identity not null,	-- auto-generated number
	lastname nvarchar(20) not null,
	firstname nvarchar(10) not null,
	title nvarchar(30) not null,
	titleofcountesy nvarchar(25) not null, -- e.g. Mr., Ms., Dr., Prof, ...
	birthdate datetime not null,
	hiredate datetime not null,
	address nvarchar(60) not null,	-- Street Number and Street Name
	city nvarchar(15) not null,
	region nvarchar(15) null,	-- Starte or Province
	postalcode nvarchar(10) not null,
	country nvarchar(15) not null,
	HomePhone nvarchar(24) null,
	Extension nvarchar(4) null,
	Photo varbinary(max) null,
	Notes nvarchar(250) null,
	ReportsTo int null,		-- Foreign Key to the same Employees table
	Departmentid smallint null,
	constraint pk_Employees primary key clustered (employeeid asc)
)
;


/***** Table No. 9 - HumanResources.Region ****/
create table HumanResources.Region
(
	regionid int identity not null,
	regiondescription nvarchar(50) not null,
	constraint pk_Region primary key clustered (regionid asc)
)
;


/***** Table No. 10 - HumanResources.Territories ****/
create table HumanResources.Territories
(
	territoryid nchar(2) not null,
	territorydescrition nchar(50) not null,
	regionid int not null,
	constraint pk_Territories primary key clustered (territoryid asc)
)
;


/***** Table No. 11 - HumanResources.EmployeeTerritories ****/
create table HumanResources.EmployeeTerritories
(
	employeeid int not null,
	territoryid nchar(2) not null,	-- Foreign key (TerritoryID in Territories table)
	constraint pk_EmployeeTerritories
		primary key clustered (employeeid asc,territoryid asc)
)
;

/***** Table No. 12 - Sales.CustomerCustomerDemo ****/
create table Sales.CustomerCustomerDemo
(
	customerid nchar(5) not null,	-- Foreign key (CustomerID in Customers table)
	customertypeid nchar(10) not null,	-- Foreign key (CustomertypeID in CustomerGeographics table)
	constraint pk_CustomerCustomerDemo
		primary key clustered (customerid asc, customertypeid asc)
)
;

/***** Table No. 13 - Sales.CustomerGeographics ****/
create table Sales.CustomerGeographics
(
	customertypeid nchar(10) not null,
	customerdesc nvarchar(250) not null,
	constraint pk_CustomerGeographics
		primary key clustered (customertypeid asc)
)
;

execute sp_tables
@table_owner = 'Production',
@table_qualifier = 'HSafeway'
;
execute sp_tables
@table_qualifier = 'HSafeway'
;