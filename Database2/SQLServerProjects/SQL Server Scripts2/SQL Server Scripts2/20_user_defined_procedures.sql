/*
	Purpose:
	Script date:
	Developed by:
*/

use Safeway20H1;
go

/* create a procedure, HumanResources.getAllEmployeesSP, that returns the employee first name, last name, and title */

create procedure HumanResources.getAllEmployeesSP
as
	begin
		select HRE.FirstName, HRE.LastName, HRE.Title
		from HumanResources.Employees as HRE;
	end
;
go

/* call the procedure HumanResources.getAllEmployeesSP */
execute HumanResources.getAllEmployeesSP
;
go

/* create a procedure, HumanResources.getAllEmployeeByNameSP, that returns 
the employee first anem, last name,. and title who have the employee number*/
create procedure HumanResources.getAllEmployeeByNameSP
(
	-- declare a parameter
	-- @parameter_name as data_type [ = expression]
	@EmployeeID as int
)
as
	begin
		select FirstName, LastName, Title
		from HumanResources.Employees
		where EmployeeID = @EmployeeID
	end
;
go

execute HumanResources.getAllEmployeeByNameSP @EmployeeID = 1;
go
execute HumanResources.getAllEmployeeByNameSP 1;
go

/* create a procedure, HumanResources.getAllEmployeeByTitleSP, that returns the employee full name, phone, and title, knowing the job title*/
alter procedure HumanResources.getAllEmployeeByTitleSP
(
	@title as varchar(20)
)
as
	begin
		select CONCAT_WS(' ',FirstName, LastName) as 'Full Name', HomePhone, Title
		from HumanResources.Employees
		where title like @title
	end
;
go
	
execute HumanResources.getAllEmployeeByTitleSP 'Sales M%';

select title
from HumanResources.Employees;
go

/* create a procedure, Production.getProductListSP,
that returns a list of products with prices that do not exceed a specific amount */
create procedure Production.getProductListSP
(
	-- declare parameters
	@ProductName as nvarchar(40),	-- Product Name
	@MaxPrice as money,	-- Product Maximum price
	@ComparePrice as money output,
	@ListPrice as money output	-- output
)
as
begin
	select PP.ProductName as 'Product' , PP.UnitPrice as 'Selling Price'
	from Production.Products as PP
		join Sales.[Order Details] as SOD
			on PP.ProductID = SOD.ProductID
	where PP.ProductName like @ProductName
		and SOD.UnitPrice < @MaxPrice

	-- populate the output parameters
	set @ListPrice =
	(
		select max(SOD.UnitPrice) as 'Maximum Price'
		from Production.Products as PP
			join Sales.[Order Details] as SOD
				on PP.ProductID = SOD.ProductID
		where SOD.UnitPrice < @MaxPrice
	)
	set @ComparePrice = @MaxPrice;
end
go

/* testing procedure Production.getProductListSP */
declare @ComparePrice as money, @SellingPrice as money
execute Production.getProductListSP '%Mishi%',100,@ComparePrice output,@SellingPrice output;


if (@SellingPrice < @ComparePrice)
	begin
		print 'These products can be purchased for less than $' +
		rtrim(cast(@ComparePrice as nvarchar(20))) + '.'
	end
else
	begin
		print 'THese prices for all products in this category exeed $' +
		rtrim(cast(@ComparePrice as nvarchar(20))) + '.'
	end
;
go