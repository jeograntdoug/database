/* purpose :self join
Script Date: January 29, 2020
Developed by: Maha M.Shawkat
*/

use Safeway20h1
;
go
select*
from HumanResources.Employees
;
go
/*Find out the list f employees who report to Andrew Fuller,Vice President, Sales with an ID equals to 2
  List first name,lastName, employee number of both employees and superVisor.*/
--use as self join when a table refrences data in itself
select 
	M.employeeID as'Manager ID',
	M.FirstName as 'Manager First Name',
	M.LastName as 'manager last Name',
	E.employeeID as'Employee ID',
	E.FirstName as 'Employee First Name',
	E.LastName as 'Employee last Name'
from HumanResources.Employees as E
inner join HumanResources.Employees as M
on e.ReportsTO=m.ReportsTo 
where M.EmployeeID=2
;
go