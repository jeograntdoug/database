IF OBJECT_ID('Sales.PrintCustomers_Cursor', 'P') IS NOT NULL 
    DROP PROCEDURE Sales.PrintCustomers_Cursor; 
GO 

CREATE PROCEDURE Sales.PrintCustomers_Cursor
(
 @Cty as NVARCHAR(15)
)
AS
BEGIN
      SET NOCOUNT ON;
 
      --DECLARE THE VARIABLES FOR HOLDING DATA.
      DECLARE @CustomerId NCHAR(5)
             ,@Name NVARCHAR(40)
             ,@Country NVARCHAR(15)
 
      --DECLARE AND SET COUNTER.
      DECLARE @Counter INT
      SET @Counter = 1
 
      --DECLARE THE CURSOR FOR A QUERY(single statement/stored procedure).
      DECLARE PrintCustomers CURSOR READ_ONLY
      FOR --or you can put exec stored procedure here instead of single statement
      SELECT CustomerId, CompanyName, Country
      FROM [Sales].[Customers]
   WHERE Country=@Cty
 
      --OPEN CURSOR.
      OPEN PrintCustomers
 
      --FETCH THE RECORD INTO THE VARIABLES.
      FETCH NEXT FROM PrintCustomers INTO
      @CustomerId, @Name, @Country
 
      --LOOP UNTIL RECORDS ARE AVAILABLE.
      WHILE @@FETCH_STATUS = 0
      BEGIN
             IF @Counter = 1
             BEGIN
                        PRINT 'CustomerID' + CHAR(9) + 'Name' + CHAR(9) + CHAR(9) + CHAR(9) + 'Country'
                        PRINT '------------------------------------'
             END
 
             --PRINT CURRENT RECORD.
             PRINT CAST(@CustomerId AS VARCHAR(10)) + CHAR(9) + CHAR(9) + CHAR(9) + @Name + CHAR(9) + @Country
    
             --INCREMENT COUNTER.
             SET @Counter = @Counter + 1
   --select a temp table to return a recordset
   --select @CustomerId, @Name, @Country
             --FETCH THE NEXT RECORD INTO THE VARIABLES.
             FETCH NEXT FROM PrintCustomers INTO
             @CustomerId, @Name, @Country
      END
 
      --CLOSE THE CURSOR.
      CLOSE PrintCustomers
      DEALLOCATE PrintCustomers
END
GO
---------------------------
exec [Sales].[PrintCustomers_Cursor] 'Mexico'
go