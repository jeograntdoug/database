/* Using union clause
Script Date: february 3, 2020
Developed by: Maha M.Shawkat
*/

use Safeway20h1
;
go 
/* Return the location of the customers and the suppliers*/
/*To create a a union query, both select statement ust have the same number of columns, and the same order*/
select CompanyName,Address,city,region,PostalCode, country,'customer' as 'status'
from Sales.Customers
where country='canada'
UNION
select CompanyName,Address,city,region,PostalCode, country,'supplier' as 'status'
from production.Suppliers
where country='canada'