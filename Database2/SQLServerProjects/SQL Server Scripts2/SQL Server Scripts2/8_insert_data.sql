/* Insert data in database 2020
Script Date: January 27, 2020
Developed by:
*/

use HSafeway;

/* 1. using bulk insert 
SYNTAX:
bulk insert schema_name.table_name
from 'path\filename.ext'
with
(
	FirstRow = number_on_first_row,
	RowTerminator = '\r\n',
	FiledTerminater = ',"' or '","' or ','
*/

bulk insert Sales.Shippers
from 'C:\Users\1898918\Documents\Database\Database2\In_Class_Activities\Shippers.txt'
with
(
	FIrstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
select *
from Sales.Shippers;

delete from Sales.Shippers;

/* 2. using insert clause
insert [into] schema_name.table [(column_name1, column_name2, ... )]
values (value1, value2, ...)
*/

alter table HumanResources.Employees
	alter column titleofcountesy nvarchar(25) null
;

/*
insert into HumanResources.Employees (firstname,lastname,title,address,city,region)
values ('Nancy', 'Davolio', 'Sales Representative','123 rue delej', 'ddo','quebec')
;
*/

insert into Production.Suppliers (CompanyName, Phone, Email)
select companyName, phone, ''
from Sales.Shippers
;

select *
from Sales.Shippers
;
