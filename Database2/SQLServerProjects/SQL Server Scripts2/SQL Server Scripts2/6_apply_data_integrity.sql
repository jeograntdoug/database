/* 
Apply Data Integrity to Tables in Safeway Database 2018
Script Date: January 24, 2020
Developed by: Your Name
*/

use HSafeway;
;
go -- include end of batch markers (go statement)


/* Integrity Types:
	1. Domain (column)
	2. Entity (row)
	3. Referential (between two columns or tables)

   Constraint Types:
   1. Primary Key (pk_)
		alter table schema_name.table_name
			add constraint pk_table_name primary key clustered (column_name asc)

   2. Foreign key (fk_)
		alter table schema_name.table_name
			add constraint fk_first_table_name_second_table_name foreign key (column_name) references second_table_name (column_name)

   3. Default (df_)
		alter table schema_name.table_name
			add constraint df_column_name_table_name default (value) for column_name

   4. Check (ck_)
		alter table schema_name.table_name
			add constraint ck_column_name_table_name check (condition)

   5. Unique (uq_)
	alter table schema_name.table_name
			add constraint uq_column_name_table_name unique (column_name)
*/


/***** Table No. 1 - Sales.Customers *****/
-- No extra constraints (only one primary key)


/***** Table No. 2 - Sales.Orders *****/

-- Foreign key constraints
/* 1) Between Sales.Orders and Sales.Customers */
alter table Sales.Orders
	add constraint fk_Orders_Customers foreign key (customerid)
	references Sales.Customers (customerid)
;

/* 2) Between Sales.Orders and Sales.Shippers */

alter table Sales.Orders
	add constraint fk_Orders_Shippers foreign key (shipvia)
	references Sales.Shippers (shipperid)
;
/* 3) Between Sales.Orders and HumanResources.Employees*/
alter table Sales.Orders
	add constraint fk_Orders_Employees foreign key (employeeid)
	references HumanResources.Employees (employeeid)
;
-- Default constraints (set Freight column value to zero)
alter table Sales.Orders
	add constraint df_freight_Orders default (0)
	for Freight
;

/***** Table No. 3 - Sales.[Order Details] *****/
-- Foreign key constraints

/* 1) Between Sales.[Order Details] and Sales.Orders */
alter table Sales.[Order Details]
	add constraint fk_OrderDetails_Orders foreign key (orderid)
	references Sales.Orders (orderid)
;

/* 2) Between Sales.[Order Details] and Production.Products*/
alter table Sales.[Order Details]
	add constraint fk_OrderDetails_Products foreign key (productid)
	references Production.Products (productid)
;

-- Default constraints
/* set the default constraint value to 0 for UnitPrice and Discount, and 1 for Quantity column */

-- 1. set the default constraint value to 0 for UnitPrice
alter table Sales.[Order Details]
	add constraint df_OrderDetails_UnitPrice
	default (0) for UnitPrice
;

-- 2. set the default constraint value to 0 for Discount
/*
alter table Sales.[Order Details]
	add constraint df_OrderDetails_discount default (0)
	for discount
;
*/
-- 3. set the default constraint value to 1 for Quantity
alter table Sales.[Order Details]
	add constraint fk_OrderDetails_Quantity
	default (1) for quantity
;

/***** Table No. 4 - Production.Products *****/

-- Foreign key constraints
/* 1) Between Production.Products and Production.Suppliers*/
alter table Production.Products
	add constraint fk_Products_Suppliers foreign key (supplierid)
	references Production.Suppliers (supplierid)
;

/* 2) Between Production.Products and Production.Categories */
alter table Production.Products
	add constraint fk_Products_Categories foreign key (categoryid)
	references Production.Categories (categoryid)
;

-- Default constraints
/* set the default value to 0 for: UnitPrice, UnitsInStock, UnitsOnOrder, ReorderLevel, and Discontinued columns in Production.Products*/

-- 1. set the default constraint value to 0 for UnitPrice
alter table Production.Products
	add constraint df_UnitPrice_Products default (0)
	for UnitPrice
;
-- 2. set the default constraint value to 0 for UnitsInStock
alter table Production.Products
	add constraint df_UnitsInStock_Products
	default (0) for UnitsInStock
;
-- 3. set the default constraint value to 0 for UnitsOnOrder
alter table Production.Products
	add constraint df_UnitsOnOrder_Products
	default (0) for UnitsOnOrder
;
-- 4. set the default constraint value to 0 for ReorderLevel
alter table Production.Products
	add constraint df_ReorderLevel_Products
	default (0) for reorderlevel
;
-- 5. set the default constraint value to 0 for Discontinued
alter table Production.Products
	add constraint df_Discontinued_Products
	default (0) for discontinued
;
-- Check constraint 
/* check that the following column values in the Products table must be >= 0: UnitPrice, ReorderLevel, UnitsInStock, UnitsOnOrder */

-- 1. check that UnitPrice >= 0
alter table Production.Products
	add constraint ck_UnitPrice_Products check ( [UnitPrice] >= 0 )
;
-- 2. check that UnitsInStock >= 0
alter table Production.Products
	add constraint ck_UnitsInStock_Products check ( [UnitsInStock] >= 0 )
;
-- 3. check that UnitsOnOrder >= 0
alter table Production.Products
	add constraint ck_UnitsOnOrder_Products check ([UnitsOnOrder] >= 0 )
;
-- 4. check that ReorderLevel >= 0
alter table Production.Products
	add constraint ck_ReorderLevel_Products check ([ReorderLevel] >= 0 )
;

/***** Table No. 5 - Production.Suppliers *****/
-- No extra constraints (only one primary key)

/***** Table No. 6 - Production.Categories *****/
-- No extra constraints (only one primary key)

/***** Table No. 7 - Sales.Shippers *****/
-- No extra constraints (only one primary key)

/***** Table No. 8 - HumanResources.Employees  *****/
-- Foreign key between Employees.EmployeeID and Employees.ReportsTo 
-- Check Birth Date to be less than current date
alter table HumanResources.Employees
	add constraint fk_employees foreign key (reportsto)
	references HumanResources.Employees (employeeid)
;

alter table HumanResources.Employees
	add constraint ck_BirthDate_Employees check ( [birthdate] < getdate() )
;
/***** Table No. 9 - HumanResources.Region  *****/
-- No extra constraints (only one primary key)

/***** Table No. 10 - HumanResources.Territories  *****/
-- Foreign key constraints
/* Between HumanResources.Territories and HumanResources.Region*/
alter table HumanResources.Territories
	add constraint fk_territories_region foreign key (regionid)
	references HumanResources.region (regionid)
;
/***** Table No. 11 - HumanResources.EmployeeTerritories  *****/
-- Foreign key constraints
/* 1) Between HumanResources.EmployeeTerritories and HumanResources.Employees*/
alter table HumanResources.EmployeeTerritories
	add constraint fk_EmployeeTerritories_Employees foreign key (employeeid)
	references HumanResources.Employees (employeeid)
;
/* 2) Between HumanResources.EmployeeTerritories and HumanResources.Territories*/
alter table HumanResources.EmployeeTerritories
	add constraint fk_EmployeeTerritories_Territories foreign key (territoryid)
	references HumanResources.Territories (territoryid)
;

/***** Table No. 12 - Sales.CustomerCustomerDemo  *****/

-- Foreign key constraints
/* 1) Between Sales.CustomerCustomerDemo and Sales.CustomerDemographics */
alter table Sales.CustomerCustomerDemo
	add constraint fk_CustomerCustomerDemo_CustomerDemographics
	foreign key (customertypeid)
	references Sales.CustomerGeographics (customertypeid)
;
/* 2) Between Sales.CustomerCustomerDemo and Sales.Customers */
alter table Sales.CustomerCustomerDemo
	add constraint fk_CustomerCustomerDemo_Customers
	foreign key (customerid)
	references Sales.Customers (customerid)
;

/***** Table No. 13 - Sales.CustomerDemographics  *****/
-- No extra constraints (only one primary key)

/* Create a unique constraint on the column ProductName in the Production.Product table */
alter table Production.Products
	add constraint uq_ProductName_Products unique (ProductName)
;