use Safeway20H1;
go

/* create a table to suppert the transaction demo */

/* clean up if the tables already exsists */
IF OBJECT_ID('SimpleOrderDetails','U') is not null
	drop table SimpleOrderDetails;
go

IF OBJECT_ID('SimpleOrders', 'U') is not null
	drop table SimpleOrders;
go

create table SimpleOrders
(
	OrderID int identity(1,1) not null,
	CustomerID nchar(5) not null,	-- Foreign key references Customers table
	EmployeeID int not null,	-- Foreign key references Employees table
	OrderDate dateTime not null,
	constraint pk_SimpleOrders primary key clustered (OrderID asc)
)
;

-- add foreign key
alter table SimpleOrders
	add constraint fk_SimpleOrders_Customers foreign key (CustomerID)
	references Sales.Customers (CustomerID)
;
go

alter table SimpleOrders
	add constraint fk_SimpleOrders_EmployeeID foreign key (EmployeeID)
	references HumanResources.Employees (EmployeeID)
;
go

create table SimpleOrderDetails
(
	OrderID int not null,	-- Foreign key references SimpleOrders table
	ProductID int not null,	-- Foreign key references Products table
	UnitPrice money not null,
	Quantity smallint not null,
	constraint pk_SimpleOrderDetails primary key clustered 
	(
		OrderID asc,
		ProductID asc
	)
);
go

-- add foerign key constraint
alter table SimpleOrderDetails
	add constraint fk_SimpleOrderDetails_SimpleOrders foreign key(OrderID)
	references SimpleOrders(OrderID);
go

alter table SimpleOrderDetails
	add constraint fk_SimpleOrderDetails_Products foreign key(ProductID)
	references Production.Products(ProductID);
go

/* execute a multi-statement batch with error */
/* NOTE: this script will cause an error */
/*
begin try
	insert into SimpleOrders (CustomerID, EmployeeID, OrderDate)
	values ('ALFKI', 1, '2020/02/27');
	insert into SimpleOrders (CustomerID, EmployeeID, OrderDate)
	values ('ALFKI', 10, '2020/02/27');

	insert into SimpleOrderDetails (OrderID, ProductID, UnitPrice, Quantity)
	values (1,2,15.59,20);

	insert into SimpleOrderDetails (OrderID, ProductID, UnitPrice, Quantity)
	values (999,78,55.59,2);
end try
begin catch
	select ERROR_NUMBER() as 'Error Number', ERROR_MESSAGE() as 'Error Message'
end catch

select *
from SimpleOrders;
select *
from SimpleOrderDetails;
*/
/* execute a multi-statement batch with error */
/* NOTE: this script will cause an error */

begin try
	begin transaction
		PRINT 'Before update:'
		PRINT 'trancount value:'
		SELECT @@trancount

		-- insert data into Sime
		insert into SimpleOrders (CustomerID, EmployeeID, OrderDate)
		values ('ALFKI', 1, '2020/02/27');
		insert into SimpleOrders (CustomerID, EmployeeID, OrderDate)
		values ('ALFKI', 10, '2020/02/27');

		insert into SimpleOrderDetails (OrderID, ProductID, UnitPrice, Quantity)
		values (1,2,15.59,20);

		insert into SimpleOrderDetails (OrderID, ProductID, UnitPrice, Quantity)
		values (999,78,55.59,2);
		
		PRINT 'After update:'
		PRINT 'trancount value:'
		SELECT @@trancount
	commit transaction
end try
begin catch
	select ERROR_NUMBER() as 'Error Number', ERROR_MESSAGE() as 'Error Message'
	rollback transaction
		PRINT 'ROLLBACK issued'
end catch

select *
from SimpleOrders;
select *
from SimpleOrderDetails;