use Safeway20H1
;

select *
from Sales.orders
;

/* update data
SYNTAX:
	update schema_name.table_name
	set column_name = expression
	where condition
*/

/* update OrderDate, RequireDate, and ShippedDate as follow:
1998 --> 2020
1997 --> 2019
1996 --> 2018
by adding 22 years
*/

/* 
	using built-in sql function
	DateAdd(DatePart, number, date)
	where DatePart:	year, mnonth, day, hour, minute, second, ...
*/

-- update OrderDate
update Sales.Orders
	set OrderDate = DateAdd(year,22,OrderDate)
-- where condition;

select *
from Sales.Orders;

-- update RequiredDate
update Sales.Orders	
	set RequiredDate = DateAdd(year,22,RequiredDate)
;

select *
from Sales.Orders;

update Sales.Orders
	set ShippedDate = DateAdd(year,22,ShippedDate)
;

/* check employee data */
select *
from HumanResources.Employees
;

/* modify the BirthDate of all employees by adding 33 years */
update HumanResources.Employees
	set BirthDate = DateAdd(year, 33, BirthDate)
;

/* Adjust the employee HireDate, so that the seniority value becomes as follow:
EMployee ID		Seniority
-----------		---------
1				8
2				8
3				7
4				6
5				5
6				4
7				3
*/

/* calculate the employee seniority useing built-in SQL Server function
DateDiff(DatePart,StartDate,EndDate)
*/
select EmployeeID, FirstName, LastName, DateDiff(year,HireDate,GETDATE() ) as 'Seniority'
from HumanResources.Employees
;

update HumanResources.Employees
	set HireDate = DateAdd(year, 20, HireDate)
	where EmployeeID in(1,2)
;

update HumanResources.Employees
	set HireDate = DateAdd(year,21, HireDate)
	where EmployeeID  = 3
;

update HumanResources.Employees
	set HireDate = DateAdd(year, 22, HireDate)
	Where EmployeeID = 4
	;

update HumanResources.Employees
	set HireDate = DateAdd(year, 22, HireDate)
	where EmployeeID = 5
	;

update HumanResources.Employees
	set HireDate = DateAdd(year, 23, HireDate)
	WHere EmployeeID = 6
	;

update HumanResources.Employees
	set HireDate = DateAdd(year, 23, HireDate)
	where EmployeeID = 7
	;

update HumanResources.Employees
	set HireDate = DateAdd(year, 24, HireDate)
	Where EmployeeID = 8
	;

update HumanResources.Employees
	set HireDate = DateAdd(year, 25, HireDate)
	where EmployeeID = 9
	;