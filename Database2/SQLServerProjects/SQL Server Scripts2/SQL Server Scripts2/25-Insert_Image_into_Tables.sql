/*
 purpose :Using VARBINARY data type to insert an image
Script Date:February 4,2020
Develoed by: Maha M. Shawkat
*/
/*Add statement that specified that the script runs in the context of the master dataBase
*/
--Switch the master dataBase
use mmSafeway
;
go
/*store ana image using T SQL
SYNTAX:
INSERT into schema_name.table_name(ImageColumn)
select BULKCOLUMN
from openRowSet(BULK 'image_path'\image_name.ext',single_Blob)
as image
*/
create table Blobs
(
ImageID int identity (1,1) not null,
ImageName  varbinary(max),
constraint pk_Blobs primary key clustered (imageID asc)
)
;
go
--single insert ststement
insert into blobs(ImageName)
select bulkcolumn --part of the syntax
from openRowSet(bulk 'C:\Users\6155202\Desktop\New folder\download.jfif',single_blob)as image
;
go

/*All the employee photos have the same name, but a suffix number.
insert these ohtos in the table*/
drop table EmployeePhoto 
;
go
/*create table EmployeePhoto 
(
	EmployeeID int identity (1,1) not null,
	photo varBinary(max) null,
	constraint pk_employeePhoto primary key clustered (EmployeeID asc)
)
;
go
set Identity_insert dbo.employeePhoto on
;
go
declare @image as int --declare @image
set @image=1--initialize @image
while(@image<10)--Looping 10 times
	begin 
		declare @sql as varchar(max)
		select @sql='Insert into employeePhoto (employeeID,photo)
		select '+convert(nvarchar(5),@image)+',BulkColumn 
		from openRowSet(bulk  ''C:\Users\6155202\Desktop\EmployeePhoto\EMPID'+
		convert(nvarchar(5),@image)+'.bmp'',single_Blob)as EmployeePhoto'
		execute (@sql)
		set @image=@image+1
	end
	select*/
	create table EmployeePhoto
(
	EmployeeID int identity(1, 1) not null,
	Photo varbinary(max) null,
	constraint pk_EmployeePhoto  primary key clustered (EmployeeID asc)
)
;
go

/* allow explicit values to be inserted into the identity colum of a table */
set IDENTITY_INSERT  dbo.EmployeePhoto on
;
go

declare @image as int -- declare @image
set @image = 1 -- initialize @image
while (@image < 10) -- looping 10 times 
	begin
		declare @sql as varchar(max)
		select @sql = 'insert into EmployeePhoto (EmployeeID, Photo) 
		select ' + convert(nvarchar(5), @image) + ', BulkColumn
		from openrowset (bulk ''C:\Users\6155202\Desktop\EmployeePhoto\EMPID' +
		convert(nvarchar(5), @image) + '.bmp'', single_blob) as EmployeePhoto'
		
		execute (@sql)
		set @image = @image + 1			
	end
;
go
