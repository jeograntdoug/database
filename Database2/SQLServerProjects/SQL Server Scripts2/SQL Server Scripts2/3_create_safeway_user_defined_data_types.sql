/*	Purpose: Create User Defined Data types
	Script Date: January 23, 2020
	Developed By:
*/

/* Add a statement that specifies that the script runs in the context of the master database */
use HSafeway
;
 -- include end of the batch marker

/* Syntax to create user-defined data types
create type [schema_name.] type_name
from system_base_type
*/

/* create BusinessAddress data type */
create type BusinessAddress
from nvarchar(40) not null
;
go
/* Create RegionCode (state or province) data type */
create type dbo.RegionCode
from nchar(2) null
;
go

/* create Contacts table */
create table Person.Contacts
(
	-- using unique identifier and NewID function
	ContactID uniqueidentifier not null default newid(),
	FirstName nvarchar(15) not null,
	LastName nvarchar(15) not null,
	Address BusinessAddress,
	Region RegionCode,
)
;
go
/* return the definition of the table Contacts */
execute sp_help 'Person.Contacts'
;

/* insert two rows in table Contacts */
insert into Person.Contacts (FirstName,LastName,Address)
values ('Smith','John','some address')
;
go

insert into Person.Contacts (FirstName,LastName,Address, Region)
values ('David','Smith', 'another address','AS')
;

select *
from Person.Contacts
;