use HSafeway
;

/* Syntax:
1. 
	Create a non_clustered index on a table (base table) or a view(virtural table)
	create object_type object_name on table_name(column_name)
	create nonclustered index index_name on table_name(column_name)

2.	create a clustered index on a table
	create clustered index index_name on table_name(column_name)
*/

/* retrieve index information on table Sales.Customers */

execute sp_helpindex 'Sales.Customers'
;
go

/* the index_id is unique only within the object */

/* check if indexes exist in the HumanResources.Employees table */
execute sp_helpindex 'HumanResources.Employees'
;
go

/* the index_id is unique only within the object */
select *
from sys.indexes
;
go

-- name: name of the index. Name is uniqye
-- index_id: The ID of the index: 0 --> Heap, 1--> Clustered, 1 --> non_clustered index
-- type: type of index: 0 --> Heap, 1--> Clustered, 2 --> Non-clustered, 3 --> XML, 4 --> Spatial
-- type_desc: description of the index
-- is_unique: 0 --> index is no t unique, 1 --> index is unique
-- is_primary_key: 1 --> index is a part of the primary key constraint

select name,index_id, type, type_desc, is_unique, is_primary_key
from sys.indexes
where object_id = OBJECT_ID('Sales.Customers')
;

/* check if indexes exist in the HumanResources.Employees table */
execute sp_helpindex 'HumanResources.Employees'
;

/* create a non_clustered index (ncl_LastName) on the employee last name */
create nonclustered index ncl_LastName on HumanResources.Employees (lastname)
;

/* remove the ncl_LastName index from the HumanResources.Employeees table*/
drop index HumanResources.Employees.ncl_LastName
;

/* add a unique non-clustered index, u_ucl_ProductName, on the Production.Products table */
execute sp_helpindex 'Production.Products'
;
create unique nonclustered index u_ucl_ProductName on Production.Products (ProductName);
drop index Production.Products.u_ucl_ProductName;

execute sp_helpindex 'Sales.CustomerCustomerDemo';