/* Purpose: Craete Safeway database
	Script Date: January 22, 2020
	Developed By:
*/

select *
from sys.objects;

execute sp_helpdb 'HSafeway'
;
go

execute sp_help 'Sales.Customers'
;
/* add statement that specifies that the script runs in the context of the master database */

-- Switch to the master databse
use master
;
go -- include end of the batch marker

/* Partial Syntax to create a databse create object_type object_name
create database database_name */
DROP DATABASE HSafeway;
/* craete database HSafeway */
CREATE DATABASE HSafeway
on primary
(
	-- 1) rows data log
	Name = 'HSafeway',
	-- 2) rows data initial file size
	size = 12 MB,
	-- 3) rows data auto growth size
	filegrowth = 4 MB,
	-- 4) rows data maimum file size
	maxsize = 100 MB, -- unlimited
	-- 5) rows data path and file name
	filename = 'c:\Program files\microsoft sql server\mssql14.mssqlserver\mssql\data\HSafeway.mdf'
)
log on
(
	-- 1) log data logical filename
	Name = 'HSafeWay_log',
	-- 2) log data initial file size
	size = 3 MB, -- 1/4 of primary file size
	-- 3) log data auto growth size
	filegrowth = 10%,
	-- 4) log data maximum file size
	maxsize = 25 MB, -- or unlimited
	-- 5) log data path and file name
	filename = 'c:\Program files\microsoft sql server\mssql14.mssqlserver\mssql\data\HSafeway_log.ldf'
)
;
go

/* return information about HSafeway database using system stored procedure.
Syntax: exec(ute) procedure_name */
execute sp_helpdb
;
go

exec sp_helpdb HSafeway
;

-- modify the maxsize of the Safeway log file to 100MB
-- switch to master database
use master;

alter database HSafeway
	modify file
		(
			name = 'HSafeway_log',
			maxsize = 100MB
		)
;

/* using some of system functions */

-- determine which activity is yours
select @@SPID as 'SP ID'
;

-- execute a system stored procedure, sp_who, using the SPID value */
execute sp_who 56
;

/* return which version of SQL server you are running. Return the connection, database context, and server information */
select @@version
;

-- switch to HSafeway

use HSafeway;

select
	USER_NAME() as 'User Name',
	DB_NAME() as 'Database Name',
	@@SERVERNAME as 'Server Name'
;

select name as username,
       create_date,
       modify_date,
       type_desc as type,
       authentication_type_desc as authentication_type
from sys.database_principals
order by username;