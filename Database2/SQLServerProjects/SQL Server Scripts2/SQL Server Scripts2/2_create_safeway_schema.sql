/* Purpose : Create Safeway Schemas
	Script Date: January 22, 2020
	Developed By:
*/

-- switch to the HSafeway database
use HSafeway
;
go
/* a schema is a distinct namespace to facilitate the separation, organization, management, and ownership of database objects.
SYNTAX:
create schema object_name authorization owner_name
*/

-- create person schema
/* Check owner_name
select
	USER_NAME() as 'User Name',
	DB_NAME() as 'Database Name',
	@@SERVERNAME as 'Server Name'
;
*/

create schema Person authorization dbo
;
go
-- 2) create HumanResources schema
create schema HumanResources authorization dbo
;
go
-- 3) create Sales schema
create schema Sales authorization dbo
;
go
-- 4) create Productions schema
create schema Production authorization dbo
;
go