-- Transact-SQL Syntax for Stored Procedures in SQL Server and Azure SQL Database  
  
/*CREATE [ OR ALTER ] { PROC | PROCEDURE } 
    [schema_name.] procedure_name [ ; number ]   
    [ { @parameter [ type_schema_name. ] data_type }  
        [ VARYING ] [ = default ] [ OUT | OUTPUT | [READONLY]  
    ] [ ,...n ]   
[ WITH <procedure_option> [ ,...n ] ]  
[ FOR REPLICATION ]   
AS { [ BEGIN ] sql_statement [;] [ ...n ] [ END ] }  
[;]  
  
<procedure_option> ::=   
    [ ENCRYPTION ]  
    [ RECOMPILE ]  
    [ EXECUTE AS Clause ]  */
	--
	--
	--
	--
	--
	--
	--

	/*return all the database in sql servse*/
	use Safeway20h1
;
go 
	execute sp_databases
	;
	go
	execute sp_datatype_info_100
	;
	go
	/*return information about data base Safeway20h1*/
	exec sp_helpdb'Safeway20h1'
	;
	go
	/*return information about specific object in a database*/
	execute sp_help @objname ='sales.customers'
	;
	/*return all tables in a specific schema**/
	execute sp_tables
	--@table_name='orders',
	@table_owner='sales'
	;
	go
	/*Return constarints applyed to the sales.oredrs table*/
	execute sp_helpconstraint 'sales.orders'
	;
	go
	/*
	1.write a query that retreive system metadata
	2.using system catalog view
	3.using standard called information_schema viesws
	4.using syste functions
	*/
	--1.write a query that retreive system metadata
	--*view the server settings using system view and functions
	select name,value,value_in_use,description
	from sys.configurations
	; 
	go
	/*return the list of all databases in sql server*/
	select name,database_id,collation_name,user_access,user_access_desc,state_desc
	from sys.databases
	;
	go
	/*list all user defined tables*/
	select
	s.name as 'schema name',
	t.name as 'table name',
	t.type_desc as 'table description',
	t.create_date as 'created date'

	from sys.tables as t
		inner join sys.schemas as s
		on t.schema_ID=s.schema_ID
		order by s.name,t.name
		;
		go
	/*2.using system catalog view*/
	select*
	from INFORMATION_SCHEMA.TABLES
	where TABLE_TYPE='base table'
	;
	go
	select *
	from INFORMATION_SCHEMA.VIEWS
	;
	go
	select* 
	from INFORMATION_SCHEMA.VIEW_COLUMN_USAGE
	where VIEW_SCHEMA='sales'
	and VIEW_NAME='ordrTotalView'
	;
	go
/*return the list of all stored procedures*/
exec sp_stored_procedures
;
go
--4.using syste functions
/*return the list of all user defined functions*/
select 
	name as 'function name',
	SCHEMA_NAME(schema_id) as 'schema name',
	type_desc as 'description',
	create_date as 'created date',
	modify_date as 'modified date'
from sys.objects
where type_desc like '%function%'
;
go
