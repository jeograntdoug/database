SQL Server Transactions
==================

As you continue to move past SELECT statements and into data modification operations with T-SQL, you must begin to consider how to structure batches that contain multiple modification statements and batches that might encounter errors.

A transaction extends a batch from a unit submitted to the database engine to a unit of work performed by the database engine. A transaction is a sequence of T-SQL statements performed in an all-or-nothing fashion by SQL Server.

Transactions are commonly created in two ways:
� Autocommit transactions. Individual data modification statements (e.g., INSERT, UPDATE, and DELETE) submitted separately from other commands are automatically wrapped in a transaction by SQL Server. These single-statement transactions are automatically committed when the statement succeeds or are automatically rolled back when the statement encounters a runtime error.
� Explicit transactions. User-initiated transactions are created through the use of transaction control language (TCL) commands that begin, commit, or roll back work based on user-issued code. TCL is a subset of T-SQL.


For example, the following batch inserts data into two tables using two INSERT statements that are part of a single order-processing operation:

INSERT INTO dbo.SimpleOrders(custid, empid, orderdate)
VALUES (68,9,'2006-07-12');
INSERT INTO dbo.SimpleOrderDetails(orderid,productid,unitprice,qty)
VALUES (1, 2,15.20,20);
GO
Business rules might dictate that an order is complete only if the data was successfully inserted into both tables.A runtime error in this batch might result in data being inserted
into one table but not the other. Enclosing both INSERT statements in a user-defined transaction provides the ability to undo the data insertion in one table if the INSERT statement in the other table fails. A simple
batch does not provide this capability.

SQL Server manages resources on behalf of transactions while the transactions are active. These resources may include locks and entries in the transaction log to allow SQL Server to undo changes made by the
transaction should a rollback be required.


While batches of T-SQL statements provide a unit of code submitted to the server, they do not include
any logic for dealing with partial success when a runtime error occurs, even with the use of structured
exception handling's TRY / CATCH blocks.
The following example illustrates this problem:
BEGIN 
   TRY
      INSERT INTO dbo.SimpleOrders(custid, empid, orderdate)
      VALUES (68,9,'2006-07-12');
      INSERT INTO dbo.SimpleOrders(custid, empid, orderdate)
      VALUES (88,3,'2006-07-15');
      INSERT INTO dbo.SimpleOrderDetails(orderid,productid,unitprice,qty)
      VALUES (1, 2,15.20,20);
 
      INSERT INTO dbo.SimpleOrderDetails(orderid,productid,unitprice,qty)
      VALUES (999,77,26.20,15);
   END TRY

   BEGIN CATCH
      SELECT ERROR_NUMBER() AS ErrNum, ERROR_MESSAGE() AS ErrMsg;
   END CATCH;


If the first INSERT statement succeeds but a subsequent INSERT statement fails, the new row in the
dbo.SimpleOrders table will persist after the end of the batch, even after the execution branches to the
CATCH block. This issue applies to any successful statements, if a later statement fails with a runtime error. 


