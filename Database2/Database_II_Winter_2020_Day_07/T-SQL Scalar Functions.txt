T-SQL Scalar Functions

Functions are routines that are used to encapsulate frequently performed logic. Rather than having to repeat all the function logic, any code that must perform the logic can call the function.

Most high-level programming languages offer functions as blocks of code that are called by name and which can process input parameters. Microsoft SQL Server has several types of functions: scalar functions, table-valued functions, and system functions. Table-valued functions can be created in two ways: inline functions and multi-statement functions.

Scalar Functions
================
Scalar functions return a single data value of the type defined in a RETURNS clause.

Inline Table-valued Functions
==============================
An inline table-valued function returns a table that is the result of a single SELECT statement. While this is similar to a view, an inline table-valued function is more flexible in that parameters can be passed to the SELECT statement within the function.

Multi-statement Table-valued Functions
======================================
A multi-statement table-valued function returns a table built by one or more Transact-SQL statements and is similar to a stored procedure. Multi-statement table-valued functions are created for the same reasons as inline table-valued functions, but are used when the logic that the function needs to implement is too complex to be expressed in a single SELECT statement. They can be called from within a FROM clause.

System Functions
=================
System functions are built-in functions provided by SQL Server to help you perform a variety of operations. They cannot be modified.

Most of the functions are scalar functions and provide the functionality that is commonly provided by functions in other high-level languages such as operations on data types (including strings and dates and times) and conversions between data types.

Aggregates such as MIN, MAX, AVG, SUM, and COUNT perform calculations across groups of rows. Many of these functions automatically ignore NULL rows.
Ranking functions such as ROW_NUMBER, RANK, DENSE RANK, and NTILE perform windowing operations on rows of data.


Scalar User-defined Functions
==============================
Unlike the definition of a stored procedure, where the use of a BEGIN�END that wraps the body of the stored procedure is optional, the body of the function must be defined in a BEGIN�END block. The function body contains the series of Transact-SQL statements that return the value.

User-defined functions are created using the CREATE FUNCTION statement, modified using the ALTER FUNCTION statement, and removed using the DROP FUNCTION statement. Even though the body of the function (apart from inline functions) must be wrapped in a BEGIN�END block, the CREATE FUNCTION must be the only statement in the batch.

You use scalar functions to return information from a database. A scalar function returns a single data value of the type defined in a RETURNS clause. The body of the function, defined in a BEGIN�END block, contains the series of Transact-SQL statements that return the value. 

Both built-in and user-defined functions fall into one of two categories: deterministic and nondeterministic. This distinction is important as it determines where a function can be used. For example, a nondeterministic function cannot be used in the definition of a calculated column.

Deterministic Functions
========================
A deterministic function is one that will always return the same result when provided with the same set of input values and for the same database state.

Non-deterministic Functions
============================
A non-deterministic function is one that may return different results for the same set of input values each time it is called, even if the database remains in the same state. 


Table-valued Functions
========================
There are two ways to create TVFs. Inline TVFs return an output table defined by a RETURN statement that is comprised of a single SELECT statement. If the logic of the function is too complex to include in a single SELECT statement, the function needs to be implemented as a multi-statement TVF.

Multi-statement TVFs construct a table within the body of the function and then return the table. They also need to define the schema of the table to be returned.

Both types of TVF can be used as the equivalent of parameterized views.

Inline TVFs
=============
You can use inline functions to achieve the functionality of parameterized views. One of the limitations of a view is that you are not allowed to include a user-provided parameter within the view when you
create it.

For inline functions, the body of the function is not enclosed in a BEGIN�END block. A syntax error occurs if you attempt to use them. The CREATE FUNCTION statement still needs to be the only statement
in the batch.

Multi-statement TVFs
======================
A multi-statement table-valued function allows for more complexity in how the table to be returned is constructed. You can use user-defined functions that return a table to replace views. This is very useful when the logic required for constructing the return table is more complex than would be possible within the definition of a view. A table-valued function (like a stored procedure) can use complex logic and multiple Transact-SQL statements to build a table.




/* use the CASE clause to return a simple expression that detemines the result */

use Northwind
go

select UnitPrice, ProductName,
'Price Range' =
	case
		when UnitPrice = 0 then 'Item not for resale'
		when UnitPrice < 50 then 'Unit Price under $50'	
		when UnitPrice between 50 and 250 then 'Unit Price under $250'	
		when UnitPrice between 250 and 1000 then 'Unit Price under $1000'	
		else 'Unit Price over $1000'	
	end
from Products
where ProductId between 1 and 10
order by ProductID
go


/* create a scalar function fnGetEmployeeAge 
that will be used to return the employee age */
if object_ID ('fnGetEmployeeAge', 'FN') is not null
	drop function fnGetEmployeeAge
go

create function fnGetEmployeeAge 
(
	@Birthdate datetime
)
returns int	
as
begin
	-- declare the return variable here 
	declare @Age int
	-- to compute the return value
	select @Age = DateDiff(yyyy, @Birthdate, getdate() )
	--  return the result to the function
	return @Age	
end
go

/* return the firt name, last name, birthdate,
 and age of employees*/
 select E.FirstName, E.LastName, E.Birthdate,
 dbo.fnGetEmployeeAge(E.Birthdate) as Age
 from Employees as E
 where E.employeeID = 1
 -- where dbo.fnGetEmployeeAge(E.BirthDate) = 65
 go
 
 select dbo.fnGetEmployeeAge('2004/5/12')
 go

/* create a function that return the order details */

create function fnGetOrderDetails
(
	@SalesOrderID int 
)

returns table 
as 
return
(
	select OD.OrderID, P.ProductName
	from [Order Details] as OD inner join Products as P
	on OD.ProductID = P.ProductID	
	where OD.OrderID = @SalesOrderID
)
go

-- call fnGetOrderDetails function
select * 
from dbo.fnGetOrderDetails(10248)
go

select * from [order details]
where OrderID = 10248
go

/* new scalar functions in SQL Server 2012 
* String functions: format() | concat()
* conversion functions: parse() | try_Parse | try_convert
* Data and time functions: DateFormats() | DateTimeFormats() | smallDateTimeFormats()

* Logical functions: Choose() | IIF() 
*/

/* return the full employee name */
select * from employees

select FirstName, LastName, (FirstName + ' ' + LastName) as 'Full Name'
from employees
go

-- or using the concat() function
select FirstName, LastName, concat(FirstName, ' ', LastName) as 'Full Name'
from employees
go

--- example of IIF()
--Iif (condition) trueValue, falseValue
declare @Salary int
declare @AvgSalary int 

set @Salary = 20000
set @AvgSalary = 10000

select IIF(@Salary > @AvgSalary, 'Salary is greater than the average salary', 'Salary is less than the average salary') as 'IIF-Salary'
go

/* check the gender of an employee */
declare @gender bit = 0
if (@gender = 1)
select 'Male' as 'Gender'
else 
select 'Female' as 'Gender'
go

-- using the case clause 
declare @gender bit = 0
select 
	case when @gender = 1 then 'Male'
	else 'Female'
	end as 'Gender'
go

-- using the IIF() function 
declare @gender bit = 0
select IIF(@gender = 1, 'Male', 'Female') as 'Gender'
go

/* create a procedure that inserts employee data */

create procedure uspInsertEmployee
(
	@FirstName nvarchar(20),
	@LastName nvarchar(40),
	@Title nvarchar(60),
	@HireDate datetime
)
as
-- section 1: define and initialize the local variables 
declare @count int
select @count = 0
-- section 2: determine whether the record already exists
select @count = count(*)
from employees
where FirstName = @FirstNAme 
and LastName = @LastName	
-- section 3: insert the record if doesn't already exist 
if (@count = 0)
	begin
		insert into employees(FirstName, LastName, Title, HireDate)
	values (@FirstName, @LastName, @Title, @HireDate)	
	print 'Employee record inserted.'	
	end
else 
	print 'Employee resord already exists.'
go

select * from employees

-- execute uspInsertEmployee 
exec uspInsertEmployee
	@FirstName = 'John', 
	@LastName = 'Smith', 
	@Title = 'Sales Representative', 
	@HireDate = '2013-02-01' 
go

select * from employees