Structure Query Language (SQL)

1) Data Definition Language (DDL) 
	
		• create object_type object_name
		create table Contacts
		• alter object_type object_name
		alter table Contacts
		• drop object_type object_name
		
2) Data Manipulation Language (DML)
		• insert
		• update
		• delete
		
3) Data Control Language (DCL)
		• Grant 
		• Deny
		• Revoke

4) Data Transaction Language (DTL)
			• Begin Transaction
			• Commit Transaction
			• Rollback Transaction
			
Online Transactional Processing (OLTP)
Online Analytical Processing (OLAP)			