/* Apply Data Integrity to Tables in Safeway Database 2018
Script Date: January 24, 2020
Developed by: Your Name
*/

use KDSafeway2020
;
go -- include end of batch markers (go statement)


/* Integrity Types:
	1. Domain (column)
	2. Entity (row)
	3. Referential (between two columns or tables)

   Constraint Types:
   1. Primary Key (pk_)
		alter table schema_name.table_name
			add constraint pk_table_name primary key clustered (column_name asc)

   2. Foreign key (fk_)
		alter table schema_name.table_name
			add constraint fk_first_table_name_second_table_name foreign key (column_name) references second_table_name (column_name)

   3. Default (df_)
		alter table schema_name.table_name
			add constraint df_column_name_table_name default (value) for column_name

   4. Check (ck_)
		alter table schema_name.table_name
			add constraint ck_column_name_table_name check (condition)

   5. Unique (uq_)
	alter table schema_name.table_name
			add constraint uq_column_name_table_name unique (column_name)
*/


/***** Table No. 1 - Sales.Customers *****/
-- No extra constraints (only one primary key)


/***** Table No. 2 - Sales.Orders *****/

-- Foreign key constraints
/* 1) Between Sales.Orders and Sales.Customers */


/* 2) Between Sales.Orders and Sales.Shippers */


/* 3) Between Sales.Orders and HumanResources.Employees*/

-- Default constraints (set Freight column value to zero)


/***** Table No. 3 - Sales.[Order Details] *****/
-- Foreign key constraints

/* 1) Between Sales.[Order Details] and Sales.Orders */


/* 2) Between Sales.[Order Details] and Production.Products*/


-- Default constraints
/* set the default constraint value to 0 for UnitPrice and Discount, and 1 for Quantity column */

-- 1. set the default constraint value to 0 for UnitPrice

-- 2. set the default constraint value to 0 for Discount

-- 3. set the default constraint value to 1 for Quantity


/***** Table No. 4 - Production.Products *****/

-- Foreign key constraints
/* 1) Between Production.Products and Production.Suppliers*/


/* 2) Between Production.Products and Production.Categories */


-- Default constraints
/* set the default value to 0 for: UnitPrice, UnitsInStock, UnitsOnOrder, ReorderLevel, and Discontinued columns in Production.Products*/

-- 1. set the default constraint value to 0 for UnitPrice

-- 2. set the default constraint value to 0 for UnitsInStock

-- 3. set the default constraint value to 0 for UnitsOnOrder

-- 4. set the default constraint value to 0 for ReorderLevel

-- 5. set the default constraint value to 0 for Discontinued

-- Check constraint 
/* check that the following column values in the Products table must be >= 0: UnitPrice, ReorderLevel, UnitsInStock, UnitsOnOrder */

-- 1. check that UnitPrice >= 0

-- 2. check that UnitsInStock >= 0

-- 3. check that UnitsOnOrder >= 0

-- 4. check that ReorderLevel >= 0


/***** Table No. 5 - Production.Suppliers *****/
-- No extra constraints (only one primary key)

/***** Table No. 6 - Production.Categories *****/
-- No extra constraints (only one primary key)

/***** Table No. 7 - Sales.Shippers *****/
-- No extra constraints (only one primary key)

/***** Table No. 8 - HumanResources.Employees  *****/
-- Foreign key between Employees.EmployeeID and Employees.ReportsTo 
-- Check Birth Date to be less than current date


/***** Table No. 9 - HumanResources.Region  *****/
-- No extra constraints (only one primary key)

/***** Table No. 10 - HumanResources.Territories  *****/
-- Foreign key constraints
/* Between HumanResources.Territories and HumanResources.Region*/

/***** Table No. 11 - HumanResources.EmployeeTerritories  *****/
-- Foreign key constraints
/* 1) Between HumanResources.EmployeeTerritories and HumanResources.Employees*/

/* 2) Between HumanResources.EmployeeTerritories and HumanResources.Territories*/


/***** Table No. 12 - Sales.CustomerCustomerDemo  *****/

-- Foreign key constraints
/* 1) Between Sales.CustomerCustomerDemo and Sales.CustomerDemographics */

/* 2) Between Sales.CustomerCustomerDemo and Sales.Customers */


/***** Table No. 13 - Sales.CustomerDemographics  *****/
-- No extra constraints (only one primary key)
