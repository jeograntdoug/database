/* Summarizing data 
Script Date: January 28, 2020
Developed by: Your Name
*/

/* add a statement that specifies the script runs in the context of the master database */

-- switch to Northwind database 
use Safeway20H1
;
go -- include end of batch markers (go statement)


/* return the quantity of each product placed in orders */
SELECT ProductID, OrderID, Quantity
from Sales.[Order Details]
;
go

/* return the total quantity of each product placed in orders */
SELECT ProductID, SUM(Quantity) AS 'Total_Quantity'
FROM Sales.[Order Details]
GROUP BY productid
order by SUM(quantity) desc
;
go

/* return the total quantity of product number 2 placed in orders */
SELECT ProductID, SUM(Quantity) AS 'Total_Quantity'
FROM Sales.[Order Details]
WHERE productid = 2 
GROUP BY productid -- Only rows that satisfy the WHERE clause are grouped

/* Using the GROUP BY Clause with theHAVING Clause */

SELECT ProductID, OrderID, Quantity
FROM Sales.[Order Details]
where ProductID = 24
;
go

SELECT ProductID, count(Quantity) AS 'Total_Quantity'
FROM Sales.[Order Details]
GROUP BY productid
HAVING Count(quantity) >=50
;
go

/* Using the GROUP BY Clause with theROLLUP Operator */
SELECT ProductID, OrderID, SUM(Quantity) AS 'Total_Quantity'
FROM Sales.[Order Details]
where ProductID in(1, 2)
GROUP BY productid, orderid
WITH ROLLUP
ORDER BY productid, orderid
;
go

/* Using the GROUP BY Clause with theCUBE Operator */
SELECT SOD.ProductID, SO.OrderID, SUM(SOD.Quantity) AS 'Total_Quantity'
FROM Sales.[Order Details] as SOD inner join Sales.Orders as SO
	on SOD.OrderID = SO.OrderID
where SOD.ProductID in (1, 2)
GROUP BY SOD.ProductID, SO.OrderID
WITH CUBE /* The CUBE operator produces two more summary values than the ROLLUP operator */
ORDER BY productid, orderid
;
go

/* Using the GROUPING Function 
1 - represents summary values in the preceding column 
0 - represents detail values in the preceding column
*/
SELECT ProductID, GROUPING(ProductID) as 'Grouping Product ID', 
OrderID, GROUPING(OrderID) as 'Grouping Order ID',
SUM(quantity) AS total_quantity
FROM Sales.[Order Details]
GROUP BY productid, orderid
WITH CUBE 
ORDER BY productid, orderid
;
go

/* Compute is deprecated  */



SELECT TOP 5 orderid, productid, quantity
FROM Sales.[order details]
ORDER BY quantity DESC
;
go

/*
WITH TIES
Returns two or more rows that tie for last place in the limited results set. You must use this argument with the ORDER BY clause. WITH TIES might cause more rows to be returned than the value specified in expression.
*/

SELECT TOP 5 WITH TIES orderid, productid, quantity
FROM Sales.[order details]
ORDER BY quantity DESC
;
go

/* 
GROUP BY ROLLUP
Creates a group for each combination of column expressions. In addition, it "rolls up" the results into subtotals and grand totals. To do this, it moves from right to left decreasing the number of column expressions over which it creates groups and the aggregation(s).
*/

SELECT SC.Country, SC.Region, SC.City, count(SO.OrderID) AS TotalOrders
FROM Sales.Customers as SC 
	inner join Sales.Orders as SO
		on SC.CustomerID = SO.CustomerID
GROUP BY ROLLUP (SC.Country, SC.Region, SC.City)
;
go


/* GROUP BY CUBE ( )
GROUP BY CUBE creates groups for all possible combinations of columns. For GROUP BY CUBE (a, b) the results has groups for unique values of (a, b), (NULL, b), (a, NULL), and (NULL, NULL).

Using the table from the previous examples, this code runs a GROUP BY CUBE operation on Country and Region.
*/
SELECT SC.Country, SC.Region, SC.City, count(SO.OrderID) AS TotalOrders
FROM Sales.Customers as SC 
	inner join Sales.Orders as SO
		on SC.CustomerID = SO.CustomerID
GROUP BY cube (SC.Country, SC.Region, SC.City)
;
go

/* GROUP BY GROUPING SETS ( )
The GROUPING SETS option gives you the ability to combine multiple GROUP BY clauses into one GROUP BY clause. The results are the equivalent of UNION ALL of the specified groups.

For example, GROUP BY ROLLUP (Country, Region) and GROUP BY GROUPING SETS ( ROLLUP (Country, Region) ) return the same results.

When GROUPING SETS has two or more elements, the results are a union of the elements. This example returns the union of the ROLLUP and CUBE results for Country and Region
*/

SELECT SC.Country, SC.Region, SC.City, count(SO.OrderID) AS TotalOrders
FROM Sales.Customers as SC 
	inner join Sales.Orders as SO
		on SC.CustomerID = SO.CustomerID
GROUP BY GROUPING SETS ( ROLLUP (SC.Country, SC.Region, SC.City), CUBE (SC.Country, SC.Region, SC.City))
;
go
