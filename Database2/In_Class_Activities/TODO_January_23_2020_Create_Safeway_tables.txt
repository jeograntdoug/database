Create FoodCo tables 
=====================
/***** Table No. 1 - Sales.Customers ****/
Primary Key: CustomerID 
/***** Table No. 2 - Sales.Order ****/ 
primary key - OrderID identity(1, 1))
/***** Table No. 3 - Production.Products ****/ 
primary key - ProductID identity (1, 1)
/***** Table No. 4 - Sales.[Order Details] ****/ 
primary key - OrderID and ProductID (composite PK)
/***** Table No. 5 - Production.Suppliers ****/
primary key - SupplierID identity (1, 1) 
/***** Table No. 6 - Production.Categories ****/ 
Primary Key: CategoryID -> identity (1, 1)
/***** Table No. 7 - Production.Shippers ****/
Primary Key: ShipperID -> identity (1, 1)
/***** Table No. 8 - HumanResources.Employees ****/
Primary Key: EmployeeID -> identity (1, 1)
/***** Table No. 9 - HumanResources.Region ****/
Primary Key: RegionID -> int
/***** Table No. 10 - HumanResources.Territories ****/
Primary Key: TerritoryID -> nvarchar
/***** Table No. 11 - HumanResources.EmployeeTerritories ****/
Primary Key: EmployeeTerritories -> int 
/***** Table No. 12 - Sales.CustomerCustomerDemo ****/
Primary Key: CustomerID and CustomerTypeID (composite PK)
/***** Table No. 13 - Sales.CustomerGeographics ****/
Primary Key: CustomerTypeID - nvarchar
