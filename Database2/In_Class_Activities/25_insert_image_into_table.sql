/* Purpose: Using varbinary data type to insert an image 
	Script Date: February 4, 2020
	Developed By: Your Name
*/

/* add a statement that specifies that the script
runs in the context of the master database */

-- switch to the Safeway database
use Safeway20H1
;
go -- include end of batch markers (go statement)

/* store an image using T-SQL
SYNTAX:
insert into schema_name.table_name (ImageColumn)
select bulkColumn
from OpenRowset(Bulk 'image_path\image_name.ext', Single_Blob) as image
*/


create table Blobs 
(
	ImageID int identity(1, 1) not null,
	ImageName varbinary(max),
	constraint pk_Blobs primary key clustered (ImageID asc)
)
;
go


-- single insert statement 
insert into Blobs(ImageName)
select bulkColumn -- part of the syntax
from Openrowset( bulk 'F:\images\flowers.jfif', single_blob) as Image
;
go

select *
from Blobs
;
go

/* all employee photos have the same name,but a suffix number. 
Insert these photos into a table
*/

create table EmployeePhoto
(
	EmployeeID int identity(1, 1) not null,
	Photo varbinary(max) null,
	constraint pk_EmployeePhoto  primary key clustered (EmployeeID asc)
)
;
go

/* allow explicit values to be inserted into the identity colum of a table */
set IDENTITY_INSERT  dbo.EmployeePhoto on
;
go

declare @image as int -- declare @image
set @image = 1 -- initialize @image
while (@image < 10) -- looping 10 times 
	begin
		declare @sql as varchar(max)
		select @sql = 'insert into EmployeePhoto (EmployeeID, Photo) 
		select ' + convert(nvarchar(5), @image) + ', BulkColumn
		from openrowset (bulk ''F:\images\EmployeePhoto\EMPID' +
		convert(nvarchar(5), @image) + '.bmp'', single_blob) as EmployeePhoto'
		
		execute (@sql)
		set @image = @image + 1			
	end
;
go

/* NOTE. the 'data_file' argument of openrwoset(bulk ...) must be a quoted string literal. It cannot be a variable or an expression. */


select *
from EmployeePhoto
;
go
