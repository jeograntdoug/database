/* Manipulate data in the Northwind database 
Script Date: January 28, 2020
Developed by: Donghyeok Seo
*/

/* add a statement that specifies the script runs in the context of the master database */

-- switch to Northwind database 
use Safeway20H1
;
go -- include end of batch markers (go statement)

/* Partial syntax:
select <select_list< [ into new_table]
[From object_name]
[Where search_condition]
[Group by group_by_condition]
[Having search_condition]
[Order by oreder_expression [asc] | [desc] ]
*/

/* 
1) Read the question
2) find the object(s) - table or a query that answer your question
3) list the column(s) - fields from each object
4) define the criteria(s) 
5) run one criteria after another
*/

/* Example 11
Suppose that you want to select a list of countries where Northwind suppliers are located. Create this query and save it as qryDistinctSuppliersCountry. 
*/
select distinct country
from Production.Suppliers;
/* Example 12
Suppose you remember that a customer's company name starts with "The", but you can't remember the rest of the name. Find all the company names starting with "The". Create this query and save it as 12_qryCompanyNameStartingWithThe.
*/

select SC.CustomerID, SC.CompanyName
from Sales.Customers as SC
where SC.CompanyName like 'The%'
;

/* Example 13
Create a query that returns the employee id, employee full name, full address, home phone, and extension. Save this query as 13_qryEmployeeFullName.
*/

select 
	HE.EmployeeID, 
	CONCAT_WS(',', HE.FirstName,HE.LastName) as 'Full Name', 
	CONCAT_WS(',',HE.Address,HE.City,HE.Region,HE.PostalCode,He.Country) as 'Full Address',
	HE.HomePhone,
	HE.Extension
from HumanResources.Employees as HE
;
/*
calculated_field_name:expression
EmployeeName:[First Name] & " " & [LastName] as "Employee"

Example 14
Suppose you want to find out what all your confection products (CategoryID = 3) would cost if you raised price by 25 percent. Add a calculated field and name it NewPrice. Create this query and save it as 14_qryNewPriceForConfectionProducts. 
*/
select *
from information_schema.columns
where table_name = 'Products'
;
select *
from production.Products;

select 
	PP.ProductID, 
	PP.ProductName, 
	PP.UnitPrice as 'Current Unit Price',
	FORMAT(PP.UnitPrice*1.25 , 'N2') as 'Raised Price(25%)'
from Production.Products as PP
where PP.CategoryID = 3
;

/* Example 15
Suppose you want to calculate the total number of Northwind products. Create this query and save it as 15_qryTotalNumberOfProducts. 
*/ 
select *
from information_schema.columns
where table_name = 'Products'
;
select COUNT (PP.ProductID) as 'Total number of Products'
from Production.Products as PP
;


/* Example 16
Suppose you want to find the total number of product ordered that come from each supplier (company name). Create this query and save it as 16_qryTotalNumberOfProductsFromEachSupplier. 
*/ 
select *
from information_schema.columns
where table_name in('Order Details','Products','Suppliers');

select PS.SupplierID, PS.CompanyName, COUNT(SOD.ProductID) as 'Total orderd Products'
from 
	Sales.[Order Details] as SOD 
	join Production.Products as PP on SOD.ProductID = PP.ProductID
	join Production.Suppliers as PS on PP.SupplierID = PS.SupplierID
group by PS.SupplierID, PS.CompanyName
;
/* Example 17
How many products each category (category Name) contains. Create this query and save it as 17_qryTotalNumberOfProductsPerCategory.
*/
select PC.CategoryID, PC.CategoryName, count(PP.ProductID) as 'Total Products'
from Production.Categories as PC
	join Production.Products as PP on PC.CategoryID = PP.CategoryID
group by PC.CategoryID, PC.CategoryName
;
/* Example 18
What is the perecentage of UnitsInStock comparing to UnitsOnOrder. Create this query and save it as 18_qryUnitPercentage.
*/
select 
	PP.ProductID, 
	PP.ProductName, 
	PP.UnitsInStock, 
	PP.UnitsOnOrder, 
	CASE
        WHEN PP.UnitsOnOrder is null 
		or (PP.UnitsOnOrder+PP.UnitsInStock) = 0
            THEN 100
			ELSE (PP.UnitsInStock * 100 / (PP.UnitsOnOrder+PP.UnitsInStock))
	END as 'Percentage(%)'
from Production.Products as PP
;
select * from Production.Products;


/* Example 19
Find the ten most expensive products. Create this query and save it as 19_qryTenMostExpensiveProducts.
*/
select top 10 ProductID, ProductName, UnitPrice
from Production.Products
order by UnitPrice DESC;

/* Example 20
calculate the subtotal of each order. Create this query and save it as 20_qryOrderSubtotal.
Subtotal = unit price x quantity - discount
*/
select 
	SO.OrderID,
	SUM(SOD.UnitPrice * SOD.Quantity) as 'Subtotal(no discount)',
	FORMAT(SUM((SOD.UnitPrice * SOD.Quantity)*(1 - SOD.discount)),'N2') as Subtotal
from Sales.Orders as SO 
	join Sales.[Order Details] as SOD on SO.OrderID = SOD.OrderID
group by SO.OrderID
;

