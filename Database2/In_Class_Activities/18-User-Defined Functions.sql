/* User-Defined Functions 
Script Date: January 30, 2020
Developed by: Your Name
*/

use Safeway20H1
;
go -- include end of batch markers (go statement)


/* A User-Defined Function (UDF) is a Transactional-SQL (T-SQL) statements that returns parameters, perform an action, such as calculation, and returns the result of that action as a value. 
The return value can either be a scalar (single) or a table. */


/* create a function, HumanResources.getEmployeeSeniorityFn, that returns the employee seniority. drop the function if exists and re-createe it */

/*
SYNTAX:
	create function schema_name.function_name
	(
		[
			@parameter_name as data_type,
			@parameter_name as data_type,
			...
		]
	)
	returns data_type
	as
		begin
			T-SQL statement goes here
		end
		return
	;
	go
*/

/* 
	if OBEJCT_ID('schema_name.object_name','object_type') is not null
		drop object_type object_name
object_type
	'U' - table
	'V' - View
	'Fn' - function
*/

/* create a function , HumanResources.getEmployeeSeniorityFn, that returns the employee
seniority. drop the function if exists and re-create it */

if OBJECT_ID('HumanResources.getEmployeeSeniorityFn', 'Fn') is not null
	drop function HumanResources.getEmployeeSeniorityFn
;
go

create function HumanResources.getEmployeeSeniorityFn
(
	-- declare a parameter
	@HireDate as datetime
)
returns int
as
	begin
		-- declare the return variagle
		declare @Seniority as int
		-- compute the return value
		select @Seniority = DATEDIFF(YEAR, @HireDate,GETDATE())
		-- return the result to the function caller
		return @Seniority
	end
;
go

/* testing the HumanResources.getEmployeeSeniorityFn function */

select HireDate,HumanResources.getEmployeeSeniorityFn(HireDate) as Seniority
from HumanResources.Employees;
go
/* create a function, Sales.getNumberOfDaysFn, that returns the number of days between the order
date and ship date. How long it took to chip an order. */

create Function Sales.getNumberOfDaysFn
(
	@orderDate as date,
	@shipDate as date
) returns int
begin
	return DATEDIFF(day,@orderDate,@shipDate)
end
;
go


select *
from information_schema.columns
where table_name = 'orders'
;

select OrderID,Sales.getNumberOfDaysFn(OrderDate,ShippedDate) as 'Shipping period'
from Sales.Orders;