/*  Using Nested (sub) Queries 
	Script Date: January 29, 2020
	Developed by: Your Name
*/ 

/* add a statement that specifies the script runs in the context of the master database */

-- switch to FoodCo2019 database 
use Safeway20H1
;
go -- include end of batch markers (go statement)

/* A subquery is a query that is nested inside a SELECT, INSERT, UPDATE, or DELETE statement, or inside another subquery. A subquery can be used anywhere an expression is allowed. 

A subquery is also called an inner query or inner select, while the statement containing a subquery is also called an outer query or outer select.

A subquery nested in the outer SELECT statement has the following components:

A regular SELECT query including the regular select list components.
A regular FROM clause including one or more table or view names.
An optional WHERE clause.
An optional GROUP BY clause.
An optional HAVING clause.

*/

/* a scalar subquery -> a query that returns a single value 
 Multiple-value subquery --> a query that returns a result set much like a single-column table 
*/

/* return the last order placed */
select top 1 SO.OrderID
from Sales.Orders as SO
order by SO.OrderID desc
;

-- using mx() function
select max(SO.OrderID) as 'Last Order'
from Sales.Orders as So
;

-- get the order from order details table
select max(SOD.OrderID) as 'Last Order'
from Sales.[Order Details] as SOD
;

/* return the last order placed with the product id, unit price, and the quantity */
select SOD.OrderID, SOD.ProductID, SOD.UnitPrice, SOD.Quantity
from (select max(SO.OrderID) as 'Last Order'
		from Sales.Orders as SO) as MO
		join Sales.[Order Details] as SOD on MO.[last Order] = SOD.OrderID
;

 
/* return the last order placed using as nested query */


/* return the list of orders placed by customers from Mexico (Ship Country) */
select *
from Sales.Orders as SO
where SO.ShipCountry = 'Mexico'
;



/* return the list of orders placed by customers (Company Name) from Mexico (Ship Country) */
select SC.CompanyName, SO.OrderID
from Sales.Orders as SO join Sales.Customers as SC on SO.CustomerID = SC.CustomerID
where SO.ShipCountry = 'Mexico'
;


select SC.CompanyName
from Sales.Customers as SC
where SC.CustomerID in(
						select SO.CustomerID
						from Sales.Orders as SO
						where SO.ShipCountry = 'Mexico'
					)
;
/* Correlated sub-query refers to columns of table used in outer table. Correlated sub-query is used to pass a value from the outer query to the inner query to be used as parameter 
*/

/* return orders with the last order date for each employee */
select *
from Sales.Orders;

select HE.EmployeeID, SO.OrderID, max(SO.OrderDate) as 'last order'
from Sales.Orders as SO join HumanResources.Employees as HE on SO.EmployeeID = HE.EmployeeID
group by HE.EmployeeID ,SO.OrderID
;


/* use the [NOT] [EXSITS] predicate with sub-query to check for any result that returns from a query. EXISTS evaluates whether rows exist, but rather than returns them, it returns true or false
*/

/* return the number of employees who are associated with orders */
select distinct SO.EmployeeID
from Sales.Orders as SO
order by SO.EmployeeID
;

select HRE.EmployeeID as 'Employee Number', HRE.LastName as 'Last Name'
from HumanResources.Employees as HRE
where
(
	select count(*)
	from Sales.Orders as SO
	where HRE.EmployeeID = SO.EmployeeID
) > 0
;

-- using the EXISTS to return the same result 
select HRE.EmployeeID as 'Employee Number', HRE.LastName as 'Last Name'
from HumanResources.Employees as HRE
where exists
(
	select count(*)
	from Sales.Orders as SO
	where SO.EmployeeID = HRE.EmployeeID
)
order by HRE.EmployeeID
;

-- add yourself as a new employee
insert into HumanResources.Employees (FirstName,LastName)
values ('haha','hoho');

select *
from HumanResources.Employees;
/* return any customer (company name) that has never placed an order */
-- use [NOT} EXISTS clause

select SC.CompanyName
from Sales.Customers as SC
where exists
(
	select SO.CustomerID
	from 
)
;
select HRE.EmployeeID as 'Employee Number', HRE.LastName as 'Last Name'
from HumanResources.Employees as HRE
where not exists
(
	select count(*)
	from Sales.Orders as SO
	where SO.EmployeeID = HRE.EmployeeID
	group by SO.OrderID
)
;
-- same query using outer join
select HRE.EmployeeID as 'Employee Number', HRE.LastName as 'Last Name'
from HumanResources.Employees as HRE 
	left join Sales.Orders as SO on HRE.EmployeeID = SO.EmployeeID
where SO.EmployeeID is null
;
