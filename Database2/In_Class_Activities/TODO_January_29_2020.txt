Querying Multiple Tables
========================

/* return the customer company name and the total number of orders placed in 2018. */

/* Find only customers (Company Name) who have placed orders. Return Company name, OrderId, and Order Date. (return matched result between Customers and Orders 
(Customers who did placed orders) */



/* Find customers (Company Name) and orders they've placed. you want to list customers even if they haven't placed any order yet. (return unmatched result between Customers and Orders (Customers who did not placed orders) */


/*
Suppose you want to find customers (Company Name) who ordered the "Chef Anton's Cajun Seasoning" product (Product Name). The information you want comes from the Customers and Products tables. These tables, however, are not related.
*/


/* Find the total number of products that come from each supplier (Company Name) */


/* Find the discontinued products. list the category name, and the product name */


