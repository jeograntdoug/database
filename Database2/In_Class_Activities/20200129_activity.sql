use Safeway20H1
;

-- switch to the tempdb
use tempdb;


/* 1. create a table with a check constraint */
create table Employees
(
	EmployeeID int not null,
	FirstName nvarchar(20) not null,
	LastName nvarchar(20) not null,
	Salary decimal(12,2),
	constraint pk_Employees primary key Clustered (EmployeeID)
)
;

/* 2. add a check constraint - Salary must be less than $120,000 */
alter table Employees
	add constraint ck_Salary_Employees check (Salary < 120000)
;

/* 3. insert a valid data (2 rows will be inserted) */
insert into Employees (EmployeeID, FirstName,LastName,Salary)
values (112211,'John', 'Smith', 65000)
;
insert into Employees (EmployeeID, FirstName,LastName,Salary)
values (221122,'David', 'Smith', 78000)
;

/* Try to insert data that violates the check constraint */
insert into Employees (EmployeeID, FirstName,LastName,Salary)
values (331133,'Pat', 'Smith', 125000)
;

/* Disable the check constraint and try again */
/* SYNTAX:
	alter table schema_name.table_name
		NoCheck conatraint constraint_name
*/
alter table dbo.Employees
	NoCheck constraint ck_Salary_Employees
;
/* Try to insert data that violates the check constraint */
insert into Employees (EmployeeID, FirstName,LastName,Salary)
values (331133,'Pat', 'Smith', 125000)
;

/* Re-enable the check constraint. Remember that NOCHECK is the default value */
alter table dbo.Employees
	Check constraint ck_Salary_Employees
;

/* Try to insert data that violates the check constraint */
insert into Employees (EmployeeID, FirstName,LastName,Salary)
values (331133,'Pat', 'Smith', 125000)
;

select * from Employees;

/* disable the check constraint and enable it again, but this time using WITH CHECK. It is not working, since the existing data is checked*/
alter table dbo.Employees
	NoCheck constraint ck_Salary_Employees;

alter table dbo.Employees
	with check check constraint ck_Salary_Employees;

delete from Employees
where EmployeeID = 331133;