/* Creating Views 
Script Date: January 29, 2020
Developed by: Donghyeok Seo
*/

use Safeway20H1
;
go -- include end of batch markers (go statement)


/* 1. create the contact customer view (Sales.ContactNameView) that contains the contact name and contact title */
if OBJECT_ID('Sales.ContactNameView','V') is not null
	drop view Sales.ContactNameView
;
go

create view Sales.ContactNameView
as
	select SC.CompanyName, SC.CustomerID, SC.ContactName, SC.ContactTitle
	from Sales.Customers as SC
;
go

select *
from Sales.ContactNameView
;
go

/* 2. modify the Sales.ContactNameView and add the country and phone */
alter view Sales.ContactNameView
as
	select SC.CompanyName, SC.CustomerID, SC.ContactName, SC.ContactTitle, SC.Phone, SC.Country
	from Sales.Customers as SC
;
go

/* 3. return the grand total of all orders placed in 2018. use the Sales.OrderTotalView in your script. */

create view Sales.OrderTotalView
as
select 
	SO.OrderID, 
	Convert(money,(SUM((SOD.UnitPrice * SOD.Quantity) * (1-SOD.Discount)))) as 'GrandTotal'
from Sales.Orders as SO 
	join Sales.[Order Details] as SOD on SO.OrderID = SOD.OrderID
where year(SO.OrderDate) = '2018'
group by SO.OrderID
;
go


/* 4. Create a view named [Product Sales for 2018]. Include category name, product name, and the sum of each order. */

create view [Product Sales for 2018]
as
select PP.ProductName, PC.CategoryName, PS.SubTotal
from Production.Products as PP
	join Production.Categories as PC on PP.CategoryID = PC.CategoryID
	join (
		select 
			SOD.ProductID, 
			FORMAT(SUM((SOD.UnitPrice * SOD.Quantity) * (1 - SOD.Discount)),'N2') as 'Subtotal'
		from Sales.[Order Details] as SOD
			join Sales.Orders as SO on SOD.OrderID = SO.OrderID
		where YEAR(SO.OrderDate) = '2018'
		group by SOD.ProductID
	) as PS on PP.ProductID = PS.ProductID
;
go
select *
from [Product Sales For 2018]
;
/*

select *
from information_schema.columns
where table_name = 'Products'
	or table_name = 'Categories'
	or table_name = 'Orders'
	or table_name = 'Order Details'
;

select PP.ProductID, PP.ProductName, PC.CategoryName
from Production.Products as PP
	join Production.Categories as PC on PP.CategoryID = PC.CategoryID
;

select SOD.ProductID, FORMAT(SUM((SOD.UnitPrice * SOD.Quantity) * (1 - SOD.Discount)),'N2') as 'Subtotal'
from Sales.[Order Details] as SOD
	join Sales.Orders as SO on SOD.OrderID = SO.OrderID
where YEAR(SO.OrderDate) = '2018'
group by SOD.ProductID
;

*/


/* 5. create a view named [Category Sales for 2018] that returns the category name, and the OrderTotal (from the OrderTotalView) */

select PC.CategoryName, ROUND(SUM(OTV.[GrandTotal]),2) as OrderTotal
from Production.Categories as PC
	join Production.Products as PP on PC.CategoryID = PP.CategoryID
	join Sales.[Order Details] as SOD on PP.ProductID = SOD.ProductID
	join Sales.OrderTotalView as OTV on SOD.OrderID = OTV.OrderID
group by PC.CategoryName
;

/* 6. create a view named Invoices, that includes the ship name, Ship Address, Ship City, Ship Region, Ship Postal Code, Ship Country, CustomerID, Customer Company name, Customer address, Customer city, Customer region, Customer postal code, Customer country, employee full name as Salesperson, OrderID, OrderDate, RequiredDate, ShippedDate, Shipper CompanyName, 
ProductID, ProductName, "Order Details".UnitPrice, "Order Details".Quantity, "Order Details".Discount, TotlatOrder from OrderTotalView, Freight
*/
select
	SO.OrderID,SO.OrderDate, SO.RequiredDate, SO.ShippedDate, PS.CompanyName, SO.Freight,
	SO.shipName,SO.shipAddress,SO.shipCity,SO.shipRegion,SO.ShipPostalCode,SO.shipCountry,
	SC.CustomerID,SC.CompanyName,SC.Address,SC.City,SC.Region,SC.Country,
	CONCAT_WS(',',HRE.FirstName,HRE.LastName) as 'SalesPerson',
	PP.ProductID,PP.ProductName,
	SOD.UnitPrice,SOD.Quantity,SOD.Discount,
	SOT.GrandTotal as 'TotalOrder'
from Sales.Orders as SO
	join Sales.[Order Details] as SOD on SO.OrderID = SOD.OrderID
	join Production.Products as PP on SOD.ProductID = PP.ProductID
	join Sales.OrderTotalView as SOT on SO.OrderID = SOT.OrderID
	join Sales.Customers as SC on SO.CustomerID = SC.CustomerID
	join HumanResources.Employees as HRE on SO.EmployeeID = HRE. EmployeeID
	join Production.Shippers as PS on SO.ShipVia = PS.ShipperID
;




select *
from information_schema.columns
where table_name in('Orders','Order Details');

/* some of system views */
select *
from sys.all_views;

select *
from sys.views;
go
/* change the column headings in the HumanResources.Employees */
create view HumanResources.EmployeeView (EmployeeNo, GivenName, FamilyName, JobTitle)
as
	select E.EmployeeID, E.FirstName, E.LastName, E.Title
	from HumanResources.Employees as E
;
go

select *
from HumanResources.EmployeeView
;

go 
/* create the order total view, Sales.OrderTotalView, that returns the sum of each order */
if OBJECT_ID('Sales.OrderTotalView', 'V') is not null
	drop view Sales.OrderTotalView
;
go

create view Sales.OrderTotalView
with encryption	-- cannot see script
as
select SOD.OrderID,
		format(SUM((SOD.UnitPrice * SOD.Quantity) * (1 - SOD.Discount)),'N2') as 'OrderTotal'
from Sales.[Order Details] as SOD
group by SOD.OrderID
;
go
/* return the definition (structure) of the Sales.OrderTotalView */
execute sp_helptext 'Sales.OrderTotalView'
;


/*
Querying Multiple Tables
========================
*/
use safeway20H1;

/* return the customer company name and the total number of orders placed in 2018. */
select SC.CompanyName, COUNT(SO.OrderID) as 'Total Orders'
from Sales.Customers as SC
	join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
where YEAR(SO.OrderDate) = 2018
group by SC.CompanyName
;

select *
from information_schema.columns
where table_name = 'Orders'
;

/* Find only customers (Company Name) who have placed orders. Return Company name, OrderId, and Order Date. 
(return matched result between Customers and Orders 
(Customers who did placed orders) */
select SC.CompanyName, SO.OrderID, SO.OrderDate
from Sales.Customers as SC
	join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
;


/* Find customers (Company Name) and orders they've placed. you want to list customers even if they haven't placed any order yet. (return unmatched result between Customers and Orders (Customers who did not placed orders) */
select SC.CompanyName, SO.OrderID
from Sales.Customers as SC 
	left join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
;


/*
Suppose you want to find customers (Company Name) who ordered the "Chef Anton's Cajun Seasoning" product (Product Name). The information you want comes from the Customers and Products tables. These tables, however, are not related.
*/
select SC.CompanyName
from Sales.Customers as SC
	join Sales.Orders as SO on SC.CustomerID = SO.CustomerID
	join Sales.[Order Details] as SOD on SO.OrderID = SOD.OrderID
	join Production.Products as PP on SOD.ProductID = PP.ProductID
where PP.ProductName = 'Chef Anton''s Cajun Seasoning'
;


/* Find the total number of products that come from each supplier (Company Name) */
select PS.CompanyName, COUNT(PP.ProductID) as 'Total Products'
from Production.Products as PP
	join Production.Suppliers as PS on PP.SupplierID = PS.SupplierID
group by PS.CompanyName
;

select *
from information_schema.columns
where table_name in('Suppliers','Products')
;
/* Find the discontinued products. list the category name, and the product name */
select PC.CategoryName, PP.ProductName
from Production.Products as PP
	join Production.Categories as PC on PP.CategoryID = PC.CategoryID
where PP.DIscontinued = 1
;


