/* User-Defined Functions 
Script Date: January 30, 2020
Developed by: Donghyeok seo
*/

use Safeway20H1
;
go -- include end of batch markers (go statement)

/* 1. 
create a function, Sales.getNumberOfDaysFn, that returnsd the number of days between the order 
date and the ship date. Drop the function if exists and then re-create it.
	Then test the function
*/

create function Sales.getNumberOfDaysFn
(
	@orderDate datetime,
	@shipDate datetime
) returns int
begin
	return dateDiff(day,@orderDate,@shipDate)
end
;
go

select OrderID, Sales.getNumberOfDaysFn(orderDate,shippedDate) as 'Days of shipping'
from Sales.Orders
;

/* 2. 
create a function, getEmployeeNameFn, that returns the full name of an employee */
create function HumanResources.getEmployeeNameFn
(
	@firstName as nvarchar(20),
	@lastName as nvarchar(20)
) returns nvarchar(40)
begin
	return CONCAT_WS(', ',@firstName,@lastName)
end
;
go


select HumanResources.getEmployeeNameFn(firstName,lastName)
from HumanResources.Employees;