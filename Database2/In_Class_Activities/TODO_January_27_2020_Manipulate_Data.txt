/* Manipulate data in the Northwind database 
Script Date: January 27, 2020
Developed by: Your Name
*/

/* add a statement that specifies the script runs in the context of the master database */

-- switch to Northwind database 
use Safeway20H1
;
go -- include end of batch markers (go statement)

/* Partial syntax:
select <select_list< [ into new_table]
[From object_name]
[Where search_condition]
[Group by group_by_condition]
[Having search_condition]
[Order by oreder_expression [asc] | [desc] ]
*/

/* 
1) Read the question
2) find the object(s) - table or a query that answer your question
3) list the column(s) - fields from each object
4) define the criteria(s) 
5) run one criteria after another
*/

/*
Example 1
Find customers in London (UK) with Sales Representative contact title. 
*/

/*
Example 2
Suppose that you want to see a list of countries where Northwind Traders� suppliers are located. You want to arrange the countries alphabetically, and within each country you want to list supplier names alphabetically. 
*/


/*
Example 3
Suppose you want to see the name and location of Northwind Traders� suppliers from Germany. 
*/


/*
Example 4
Suppose that you want to display company names of suppliers from Sweden, but you only want to see the company names, not the country, in the result set. 
*/


/*
Example 5
Suppose that you want to view all the fields in the Northwind Order Details table, but you�re interested in seeing only those records with an Order ID greater than 11000. 
*/



/*
Example 6
Suppose that you want to find employees how are hired between January 1st and March 31st 2018 (first quarter of 2018).   
*/




/*
Example 7
Suppose you want to see Northwind suppliers from Germany or Canada. 
*/


/*
Example 8
Suppose you want to see Northwind suppliers in either the UK (United Kingdom) or Paris.  
*/


/*
Example 9
Suppose that you want to find suppliers who have a fax number. 
*/


/*
Example 10
Suppose that you want to see Northwind Traders customers who are located in Seattle, Kirkland, or Portland. 
*/


/*
Example 11
Suppose that you want to select a list of countries where Northwind suppliers are located.
*/

