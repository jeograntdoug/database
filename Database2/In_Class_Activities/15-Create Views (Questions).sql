/* Creating Views 
Script Date: January 29, 2020
Developed by: Your Name
*/

use Safeway2020
;
go -- include end of batch markers (go statement)


/* 1. create the contact customer view (Sales.ContactNameView) that contains the contact name and contact title */


/* 2. modify the Sales.ContactNameView and add the customer id, company name, and phone */


/* 3. return the grand total of all orders placed in 2018. use the Sales.OrderTotalView in your script. */


/* 4. Create a view named [Product Sales for 2018]. Include category name, product name, and the sum of each order. */

/* 5. create a view named [Category Sales for 2018] that returns the category name, and the OrderTotal (from the OrderTotalView) */

/* 6. create a view named Invoices, that includes the ship name, Ship Address, Ship City, Ship Region, Ship Postal Code, Ship Country, CustomerID, Customer Company name, Customer address, Customer city, Customer region, Customer postal code, Customer country, employee full name as Salesperson, OrderID, OrderDate, RequiredDate, ShippedDate, Shipper CompanyName, 
ProductID, ProductName, "Order Details".UnitPrice, "Order Details".Quantity, "Order Details".Discount, TotlatOrder from OrderTotalView, Freight
*/


