/* Apply data integrity to the KDFlix2020 database  
 	Script Date: January 16, 2020
    Developed By: Your Name
*/

-- switch to the current database (KDFlix2020)
-- use database_name
use KDFlix2020
;

/* Constraint type:
1. primary key --> pk_table_name (pk_Customers)
2. foreign key --> fk_table_name1_table_name2 (fk_Orders_Customers)
3. unique --> uq_column_name_table_name (uq_CompanyName_Suppliers)
4. default --> df_column_name_table_name (df_City_Customers)
5. check -->ck_column_name1_column_name2 (ck_OrderDate_ShippedDate)
*/

/* add a primary key to an existing table 
alter table table_name
	add constraint pk_table_name primary key [clustered] (column_name [asc])
*/

/* add a default constraint  to a column when a table is already exists 
alter table table_name
	-- in MySQL
	alter column column_name
    set default 'value'
    
    -- in Oracle
alter table table_name
	modify column_name default 'value'
    
    -- In Microsoft SQL Server
alter table table_name
		alter column column_name
        set default 'value'
*/


/* add froeign key constraint(s) to the DVDs table */

/* 1. Between DVDs and MovieTypes tables */

/* 2) Between DVDs and Studios tables */

/* 3) Between DVDs and Ratings tables */

/* 4) Between DVDs and Formats tables */

/* 5) Between DVDs and Status tables */


/* add foreign key constraint(s) to the table DVDParticipant */

/* 1) Between DVDParticipant and DVDs tables */
    
/* 2) Between DVDParticipant and Participants tables */

/* 3) Between DVDParticipant and Roles tables */


/* add foreign key constraint(s) to the table Orders */

/* 1) Between Orders and Customers tables */
        
/* 2) Between Orders and Employees tables */


/* add foreign key constraint(s) to the table Transactions */

/* 1) Between Transactions and Orders tables */
   
/* 2) Between Transactions and DVDs tables */



/*
Foreign keys in each table
dvdparticipant - 3
dvds - 5
orders - 2
transactions - 2
*/

/* set the DVD name to unique constraint */

/* check constraint
Syntax:
	alter table table_name
		add constraint ck_column_name_table_name check (condition)
*/

/* set a check constraint to the Transactions table 
on Date Due to be greater than or equal to Date Out */




