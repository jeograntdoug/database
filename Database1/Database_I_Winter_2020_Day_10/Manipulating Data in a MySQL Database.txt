﻿Manipulating Data in a MySQL Database
=====================================

Inserting Data in a MySQL Database
===================================
Before you can do anything with the data in a database, the data must exist. For this reason, the first SQL statements that you need to learn are those that insert data in a MySQL database. When you add data to a database, you're actually adding it to the individual tables in that database. Because of this, you must remain aware of how tables relate to each other and whether foreign keys have been defined on any of their columns.


MySQL supports two types of statements that insert values in a table: INSERT and REPLACE. Both statements allow you to add data directly to the tables in your MySQL database. MySQL also supports other methods for inserting data in a table, such as copying or importing data.


<insert statement>::=
INSERT [LOW_PRIORITY | DELAYED] [IGNORE] [INTO]
{<values option> | <set option> | <select option>}
<values option>::=
<table name> [(<column name> [{, <column name>}...])]
VALUES ({<expression> | DEFAULT} [{, {<expression> | DEFAULT}}...])
[{, ({<expression> | DEFAULT} [{, {<expression> | DEFAULT}}...])}...]
<set option>::=
<table name>
SET <column name>={<expression> | DEFAULT}
[{, <column name>={<expression> | DEFAULT}}...]
<select option>::=
<table name> [(<column name> [{, <column name>}...])]
<select statement>



INSERT [LOW_PRIORITY | DELAYED] [IGNORE] [INTO]

The first line includes the required INSERT keyword and a number of options. The first of these options are LOW_PRIORITY and DELAYED. You can include either one of these options, but you cannot include both. If you specify LOW_PRIORITY, the statement is not executed until no other client connections are accessing the same table that the INSERT statement is accessing.



CREATE TABLE CDs
(
CDID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
CDName VARCHAR(50) NOT NULL,
Copyright YEAR,
NumberDisks TINYINT UNSIGNED NOT NULL DEFAULT 1,
NumberInStock TINYINT UNSIGNED,
NumberOnReserve TINYINT UNSIGNED NOT NULL,
NumberAvailable TINYINT UNSIGNED NOT NULL,
CDType VARCHAR(20),
RowAdded TIMESTAMP
);



INSERT INTO CDs
VALUES (NULL, 'Ain\'t Ever Satisfied: The Steve Earle Collection',
1996, 2, 10, 3, NumberInStock-NumberOnReserve, 'Country', NULL);

You can also use the backslash to specify other literal values, such as double quotes (\"), a backslash (\\), a percentage sign (\%) or an underscore (\_).


Using the <set option> Alternative of the INSERT Statement
============================================================
The <set option> method for creating an INSERT statement provides you with an alternative to the <values option> method when you're adding only one row at a time to a table.

INSERT INTO CDs (CDName, Copyright, NumberDisks,
NumberInStock, NumberOnReserve, NumberAvailable, CDType)
VALUES ('Blues on the Bayou', 1998, DEFAULT,
4, 1, NumberInStock-NumberOnReserve, 'Blues');


You could rewrite the statement by using the <set option> alternative

INSERT DELAYED INTO CDs
SET CDName='Blues on the Bayou', Copyright=1998,
NumberDisks=DEFAULT, NumberInStock=4, NumberOnReserve=1,
NumberAvailable=NumberInStock-NumberOnReserve, CDType='Blues';


Using a REPLACE Statement to Add Data
========================================
In addition to using an INSERT statement to add data to a table, you can also use a REPLACE statement. A REPLACE statement is similar to an INSERT statement in most respects. The main difference between the two is in how values in a primary key column or a unique index are treated. In an INSERT statement, if you try to insert a row that contains a unique index or primary key value that already exists in the table, you aren't able to add that row. A REPLACE statement, however, deletes the old row and adds the new row.

CREATE TABLE Inventory
(
ProductID SMALLINT UNSIGNED NOT NULL PRIMARY KEY,
NumberInStock SMALLINT UNSIGNED NOT NULL,
NumberOnOrder SMALLINT UNSIGNED NOT NULL,
DateUpdated DATE
);

The following REPLACE statement adds values to each column in the Inventory table:
REPLACE LOW_PRIORITY INTO Inventory
VALUES (101, 20, 25, '2004-10-14');

Using the <set option> Alternative of the REPLACE Statement
===========================================================
REPLACE INTO Inventory (ProductID, NumberInStock, DateUpdated)
VALUES (107, 16, '2004-11-30');

Notice that values are specified for the ProductID, NumberInStock, and DateUpdated columns, but not for the NumberOnOrder column. Because this column is configured with an integer data type and because the column does not permit null values, a 0 value is added to the column.



Updating Data in a MySQL Database
=================================

<update statement>::=
UPDATE [LOW_PRIORITY] [IGNORE]
<single table update> | <joined table update>
<single table update>::=
<table name>
SET <column name>=<expression> [{, <column name>=<expression>}...]
[WHERE <where definition>]
[ORDER BY <column name> [ASC | DESC] [{, <column name > [ASC | DESC]}...]]
[LIMIT <row count>]
<joined table update>::=
<table name> [{, <table name>}...]
SET <column name>=<expression> [{, <column name>=<expression>}...]
[WHERE <where definition>]


UPDATE Books
SET InStock=InStock+10;


UPDATE Orders
SET Quantity=2
WHERE OrderID=1001;


UPDATE Orders
SET Quantity=Quantity+1
WHERE OrderID=1001;



Using a DELETE Statement to Delete Data
=======================================
The DELETE statement is the primary statement that you use to remove data from a table.

<delete statement>::=
DELETE [LOW_PRIORITY] [QUICK] [IGNORE]
{<single table delete> | <from join delete> | <using join delete>
<single table delete>::=
FROM <table name>
[WHERE <where definition>]
[ORDER BY <column name> [ASC | DESC] [{, <column name> [ASC | DESC]}...]]
[LIMIT <row count>]
<from join delete>::=
<table name>[.*] [{, <table name>[.*]}...]
FROM <table name> [{, <table name>}...]
[WHERE <where definition>]
<using join delete>::=
FROM <table name>[.*] [{, <table name>[.*]}...]
USING <table name> [{, <table name>}...]
[WHERE <where definition>]



DELETE FROM Orders;

DELETE FROM Orders
WHERE OrderID=1020;

DELETE LOW_PRIORITY FROM Orders
WHERE BookID=103
ORDER BY DateOrdered DESC
LIMIT 1;






Exporting data
===============

SELECT CDName, InStock, Category
INTO OUTFILE 'CDsOut.txt'
FROM CDs;


SELECT CDName, InStock, Category INTO OUTFILE 'CDsOut.txt'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
FROM CDs;


Exporting Data to a Dump File
==============================
Exporting data to a dump file is much simpler than an out file because you cannot specify any FIELDS or LINES subclauses.

INTO DUMPFILE '<filename>'


SELECT CDName, InStock, Category
INTO DUMPFILE 'CDsOut.txt'
FROM CDs
WHERE CDID=110;


Importing Data into a Table
===========================

<load data statement>::=
LOAD DATA [LOW_PRIORITY | CONCURRENT] [LOCAL] INFILE '<filename>'
[REPLACE | IGNORE]
INTO TABLE <table name>
[<import option> [<import option>]]
[IGNORE <number> LINES]
[(<column name> [{, <column name>}...])]


CREATE TABLE CDs3
(
CDName VARCHAR(50) NOT NULL,
InStock SMALLINT UNSIGNED NOT NULL,
Category VARCHAR(20)
);


SELECT CDName, InStock, Category INTO OUTFILE 'CDsBlues.sql'
FROM CDs WHERE Category='Blues';


LOAD DATA INFILE 'CDsBlues.sql'
INTO TABLE CDs3;


LOAD DATA INFILE 'CDsCountry.sql'
INTO TABLE CDs3
FIELDS
TERMINATED BY ','
ENCLOSED BY '"';




