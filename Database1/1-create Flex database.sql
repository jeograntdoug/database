/* Script Data: January 15, 2020
Developed by: Heok
*/

/* PARTIAL SYNTAX
create object_type object_name
where object_type: database, table, view, function, procedure, ...
*/

/* check if the database exises. If so, delete it
*ONLY ON THE DEVELOPMET ENVIREMENT) and then re-create it. */

CREATE DATABASE IF NOT EXISTS HFlix2020
;

-- switch to the HFlix database
use HFlix2020
;

/* using the schma clause to create a database. Create a schema is
the same syntax as create database in MySQL,
but NOT IN Microsoft SQL Server
*/

CREATE SCHEMA mydb1
;

/* delete a database
SYNTAX: drop database database_name
*/
drop database HFlix2contacts020
;
drop database mydb1
;