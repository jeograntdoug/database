

/* switch to the current active database (HFlix2020) */
use HFlix2020;

/* COMPARISON AND CONVERTING FUNCTIONS
greatest() | least() | coalesce() | isNull() | interval() | strCmp() | ...
*/

/* CONTROL FLOW FUNCTIONS
if() | isNull)_ | nullIf() | case() | cast()
*/

/* STRING FUNCTIONS
ASCII() | char_length() | concat() | concat_ws() | lcase() | lower()
| ucase() | upper() | left() | right() | substring() | ...
*/

/* NUMERIC FUNCTIONS
floor() | mod() | PI() | pow() | power() | round() | sqrt() | ...
*/

/* DATE AND TIME FUNCTIONS
addDate() | date_add() | subDate() | date+sub() | sxtranct() | surDate() | current_date()
| curTime() | current_time() | current_timestamp()_ | now() | date() | time()| year() | month() 
| day() | monthName() | dateDiff() | timeDiff() | hour() | minute() | second() | ...
*/

/* AGGREGATE (SUMMARY) FUNCTIONS
sum() | min() | max() | count() | ...
*/

/*ENCRYPTION FUNCTIONS
encode() | decode() | password() | md5() | sha() | sha1() | current_user()
| session_user() | system_user() | user() | connection_id() | database() | version()
| found_rows() | last_insert_id() | ...
*/


/* what is the greatest value of 2,3,10 */
SELECT greatest(2,3,10) AS 'The Greatest Value';

SELECT greatest('b','bb','aa') AS 'The Greatest Char';

/* the coalesce function returns the first value in the list that is not null */
SELECT coalesce(null,'',4) AS `Coalesce First Value`;

/* isNull() returns a value of 1 if the expression is true, otherwise it return zero*/
SELECT isnull(null) as `isNull() Function`;

SELECT DateIN FROM Transactions
WHERE TransID = 1;

/* strcomp compares string values. It returns zero if the expression1 equals expression2
if expr1 < expr2 ==> -1
if expr1 > expr2 ==> 1
if expr1 = expr2 ==> 0
*/

SELECT strcmp('big','bigger') AS `strCmp Function`;

/* If( (condition), 'true_value', 'false_value') */
SELECT @grade := 78;
SELECT IF ( (@grade >= 60), 'Passed the course', 'Failed the course') AS `if Functions`;

/* show the status description from the table DVD */
SELECT 
	DVDName, 
    StatID, 
    IF( 
		(StatID = 's1'),'CheckOut', IF( (StatID = 's2'),'Available','N/A')
	) AS Status
FROM DVDs
;

SELECT *
FROM status
;

/* ifNull() evaluates the expression whether it is equal to null */
SELECT ifnull( (null), 'Incorrect Expression') as `IsNull Function`;

/* nullIF() returns null if expression1 equals to expression2, Otherwise it returns expression1 */
SELECT nullIf( 1,2) AS `NullIf Function`;

/* Some of string functions: left([column_name],number), right([column_name],number) and substring([column_name],start,length) */

/* return the string white, christmas, and chris from the dvd id = 1 */
SELECT
	left(DVDName,5) AS `Left Function`,
    right(DVDName,9) AS `Right Function`,
	substring(DVDName,7,5) AS `Substring Function`,
    mid(DVDName, 7, 5) AS `Mid Function`
FROM DVDs
WHERE DVDID = 1
;

SELECT EmpID, concat_ws(' ',EmpFN, coalesce(EmpMN,''), EmpLN) AS FullName
FROM Employees;

/* create a registration employee id based on the first 2 characters of the first name
and 3 characters of the last name*/

SELECT 
	concat_ws(' ',EmpFN, coalesce(EmpMN,''), EmpLN) AS FullName,
	Ucase( concat(left(EmpFN,2),left(EmpLN,3)) ) AS Registration_ID
FROM Employees;

use sakila;

SELECT rating, SUM(length) AS TOTALLENGTH, COUNT(length) AS count

FROM film
GROUP BY rating
HAVING SUM(length) > (SELECT AVG(length) FROM film);


use HFlix2020;

/* return the DVD name, and movie type description */
SELECT D.DVDName, M.MTypeDescrip
FROM DVDs AS D JOIN MovieTypes AS M
ON D.MTypeID = M.MTypeID
;

/* return the DVD name, movie type description, and studio description */
SELECT D.DVDName, M.MTypeDescrip, S.StudDescip
FROM DVDs AS D 
	JOIN MovieTypes AS M
	ON D.MTypeID = M.MTypeID
    JOIN Studios AS S
    ON D.StudID = S.StudID
;

SELECT C.CustFN, D.DVDName
FROM DVDs AS D 
	JOIN Transactions AS T
		ON D.DVDID = T.DVDID
	JOIN Orders AS O
		ON T.OrderID = O.OrderID
	JOIN Customers AS C
		ON O.CustID = C.CustID
WHERE T.DateIN IS NOT NULL;

SELECT * FROM Transactions
;

SELECT D.DVDName,R.RatingDescrip,
	CASE
		WHEN (D.RatingID = 'R') THEN 'Under 17 requires an adult permission'
        WHEN (D.RatingID = 'X') THEN 'No one 17 and Under'
        WHEN (D.RatingID = 'NR') THEN 'Use discretion when renting'
        else 'OK to rent minors'
	END AS Permission
FROM DVDs AS D JOIN Ratings AS R ON D.RatingID = R.RatingID
;

/* some date functions */
SELECT TransID, DateOut,
	date(DateOut) AS `The Date`,
    day(DateOut) AS `The Day`,
    month(DateOut) AS `The Month`,
    year(DateOut) AS `The Year`,
    dayname(DateOut) AS `The Day Name`,
    dayofmonth(DateOut) AS `Day of Month`,
    dayofweek(DateOut) AS `Day of Week`,
    dayofYear(DateOut) AS `Day of Year`,
    MonthName(DateOut) AS `Month Name`
FROM Transactions
;

/* DateDiff(<endDate>,<startDate>)
	TimeDiff(<endTime>,<startTime>)
*/

/* how long it took to return a dvd? */
SELECT T.DVDID, D.DVDName, T.DateOut, T.DateIn, DateDiff(T.DateIN,T.DateOut) AS `Period of rental`
FROM Transactions AS T
	JOIN DVDs AS D ON T.DVDID = D.DVDID
;

SELECT *
FROM Transactions
WHERE DateIN IS NULL;

/* how many orders placed by each customer (company name).
Sort then from hightest to lowest number */

SELECT C.CustFN, COUNT(O.OrderID) AS `Amount`
FROM Orders AS O JOIN Customers AS C ON O.CustID = C.CustID
GROUP BY C.CustFN
ORDER BY `Amount` DESC
;