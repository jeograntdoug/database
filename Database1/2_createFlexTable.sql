/*
Purpose: to create the Flix database tables
*/

/* Script Data: January 15, 2020
Developed by:
*/
DROP DATABASE HFlix2020;
CREATE DATABASE HFlix2020
;

-- switch to the HFlix2020
-- SYNTAX: use database_name;
use HFlix2020
;

/* create a tabel object
PARTIAL SYNTAX
create table table_name
(
	column_name data_type constraint(s),
    column_name data_type constraint(s),
    ...
    column_name data_type constraint(s)
)
;
where constraint defines the business rules: null, not null,
primary key, ...
*/

/* check if the table exists. If so, delete it and then re-create it.
NOTE: use this command only during the development process
NOT in a production database
*/

/***** Table No. 1 - Customers *****/
CREATE TABLE Customers
(
	-- column_name date_type constraints
    CustID SMALLINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    CustFN VARCHAR(20) NOT NULL,
    CustMN VARCHAR(20) NULL,
    CUstLN VARCHAR(20) NOT NULL
)
;

/*
Modify the CustMN and set the filed size to varchar(10).
SYNTAX: ALTER object_type object_name
*/
ALTER TABLE Customers
	modify column CustMN VARCHAR(10) NULL
;


/* show the definition (structure) of table Customers */
SHOW TABLES;

SHOW CREATE TABLE Customers
;

DESCRIBE Customers
;

SHOW COLUMNS FROM Customers
;

-- create lookup tables
/***** Table No. 2 - Roles *****/
CREATE TABLE Roles
(
	RoleID VARCHAR(4) PRIMARY KEY,
    RoleDescrip VARCHAR(30) NOT NULL
)
;

/***** Table No. 3 - MonvieTypes *****/
CREATE TABLE MovieTypes
(
	MTypeID VARCHAR(4) NOT NULL,
    MTypeDescrip VARCHAR(30) NOT NULL
)
;


/***** Table No. 4 - Studios *****/
CREATE TABLE Studios
(
	StudID VARCHAR(4) NOT NULL PRIMARY KEY,
    StudDescip VARCHAR(40) NOT NULL
)
;

/***** Table No. 5 - Ratings *****/
CREATE TABLE Ratings
(
	RatingID VARCHAR(2) PRIMARY KEY,
    RatingDescrip VARCHAR(30) NOT NULL
)
;

/* modify the column RatingID data type to VARCHAR(4) */
ALTER TABLE Ratings
	MODIFY COLUMN RatingID VARCHAR(4)
;

/***** Table NO. 6 - Formats *****/
CREATE TABLE Formats
(
	FormID CHAR(2) NOT NULL PRIMARY KEY,
    FormDescrip VARCHAR(15) NOT NULL
)
;

/***** Table No. 7 - Status *****/    
CREATE TABLE Status
(
	StatID CHAR(2) NOT NULL PRIMARY KEY,
    StatDescrip VARCHAR(20) NOT NULL
)
;

/***** Table No. 8 Participants *****/
CREATE TABLE Participants
(
	PartID SMALLINT AUTO_INCREMENT PRIMARY KEY,
    PartFN VARCHAR(20) NOT NULL,
    PartMN VARCHAR(20) NULL,
    PartLN VARCHAR(20) NOT NULL,
    RoleID VARCHAR(4) NOT NULL
)
;

/* remove the column RoleID from table Participants */
ALTER TABLE Participants
	DROP COLUMN RoleID
;

/***** Table No. 9 - DVDs *****/
CREATE TABLE DVDs
(
	DVDID SMALLINT NOT NULL PRIMARY KEY,
    DVDName VARCHAR(60) NOT NULL,
    NumDisks TINYINT NOT NULL,
    YearRlsd YEAR NOT NULL,
    MTypeID VARCHAR(4) NOT NULL,	-- Foreign key(MovieTypes)
    StudID VARCHAR(4) NOT NULL,		-- Foreign key(Studios)
    RatingID VARCHAR(4) NOT NULL,	-- Foreign key(Ratings)
    FormID VARCHAR(2) NOT NULL,		-- Foreign key(Formets)
    StatID CHAR(3) NOT NULL			-- Foreign key(Status)
)
;

/***** Table No. 10 - DVDParticipants *****/
CREATE TABLE DVDParticipants
(
	DVDID SMALLINT NOT NULL,	-- Foreign key(DVDs)
    PartID SMALLINT NOT NULL,	-- Foreign key(Participants)
    RoleID VARCHAR(4) NOT NULL,	-- Foreign key(Roles)
    CONSTRAINT pk_DVDParticipants PRIMARY KEY CLUSTERED
    (
		DVDID asc,
        PartID asc,
        RoleID asc
    )	-- composite primary key
)
;

/**** Table No. 10 - Transactions *****/
CREATE TABLE Transactions 
(
	TransID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    OrderID INT NOT NULL,	-- Foreign key(Orders)
    DVDID SMALLINT NOT NULL,	-- Foreign key(DVDs)
    DateOut DATE NOT NULL,
    DateDue DATE NOT NULL,
    DateIN DATE NOT NULL
)
;

/***** Table No. 11 - Orders *****/
CREATE TABLE Orders
(
	OrderID INT AUTO_INCREMENT PRIMARY KEY,
    CustID SMALLINT NOT NULL,	-- Foreign key(Customers)
    EmpID SMALLINT NOT NULL		-- Foreign key(Employees)
)
;

/***** Table No. 12 - Employees *****/
CREATE TABLE Employees
(
	EmpID SMALLINT AUTO_INCREMENT,
    EmpFN VARCHAR(20) NOT NULL,
    EmpMN VARCHAR(20) NOT NULL,
    EmpLN VARCHAR(20) NOT NULL,
    
    CONSTRAINT pk_Employees PRIMARY KEY employees (EmpID ASC)
)
;


SELECT *
FROM information_Schema.TABLES
WHERE table_type = 'BASE TABLE'
AND table_schema = 'HFlix2020'
;

/* how many tables exist in HFlix2020 database */
Select COUNT(*) AS 'No. of Tables in HFlix2020'
FROM information_Schema.TABLES
WHERE table_type = 'BASE TABLE'
AND table_schema = 'HFlix2020'
;

/* return the definition of table Customers */
SELECT *
FROM information_Schema.COLUMNS
WHERE TABLE_NAME = 'Customers'
AND table_schema = 'HFlix2020'
;

-- or 
SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_KEY, COLUMN_DEFAULT
FROM information_schema.COLUMNS
WHERE TABLE_NAME = 'Customers'
AND table_schema = 'HFlix2020'
;


-- ALTER TABLE DVDs
-- 	MODIFY COLUMN YearRlsd char(4) NOT NULL
-- ;

-- ALTER TABLE DVDParticipants
-- 	DROP FOREIGN KEY fk_DVDParticipants_DVDs
-- ;
-- ALTER TABLE Transactions
-- 	DROP FOREIGN KEY fk_Transactions_DVDs
-- ;
-- ALTER TABLE DVDs
-- 	DROP DVDID
-- ;

-- ALTER TABLE DVDs
-- 	ADD DVDID SMALLINT AUTO_INCREMENT PRIMARY KEY
-- ;
-- ALTER TABLE DVDParticipants
-- 	ADD CONSTRAINT pk_DVDParticipants_DVDs FOREIGN KEY (DVDID)
-- 		REFERENCES DVDs (DVDID)
-- ;

-- ALTER TABLE Transactions
-- 	ADD CONSTRAINT fk_Transactions_DVDs FOREIGN KEY (DVDID)
-- 		REFERENCES DVDs (DVDID)
-- ;

-- ALTER TABLE Transactions
-- 	MODIFY DateIN DATETIME NULL
--     ;

-- ALTER TABLE Employees
-- 	MODIFY COLUMN EmpMN VARCHAR(20) NULL
-- ;

-- set the primary key for table MovieTypes
-- ALTER TABLE MovieTypes
-- 	ADD CONSTRAINT movietypesPRIMARYpk_MovieTypes PRIMARY KEY (MTypeID)
-- ;


-- /* Drop the table */
-- DROP DATABASE HFlix2020
-- ;