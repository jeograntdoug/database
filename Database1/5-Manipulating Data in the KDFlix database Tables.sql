/*
Purpose: Manipulating Data in the HFlix database Tables.

Script Date: January 17, 2020
Developed By: Your Name
*/

/* switch to the current active database (KDFlix) */
use HFlix2020
;

/* to answer the question about your data, follow these steps:
	1. read and understand the question
    2. select the object(s), table or view, from where the data comes
    3. select the column(s) from each object
    4. define criteria(s) - conditions, and run them one after another
*/


/* 1. list all dvd id, name, and year released */
SELECT DVDID, DVDName, YearRlsd
FROM DVDs
;


/* 2. list all dvd id and names released in 2001 */
SELECT DVDID, DVDName, YearRlsd
FROM DVDs
WHERE YearRlsd = '2001'
;

/* 3. List all dvd id and names that have more than one disk */
SELECT DVDID, DVDName, NumDisks
FROM DVDs
WHERE NumDisks > 1
;

-- changing the caption - Alias
-- re-write the previous script using aliases


/* using the aliases, change the column names in the result set of 
the table Employees. For example, EmpID - Employee ID, 
EmpFN - First Name, and so on. */

SELECT EmpID AS 'Employee ID', EmpFN AS 'First Name', EmpLN AS 'Last Name'
FROM Employees
;

/* return the employee full name as a single string, 
using concatenation operator (+) */
SELECT EmpID AS 'Employee ID', EmpLN +", " + EmpFN AS 'Full Name'
FROM Employees
;
-- Doesn't suport

/* using the concat() function */
SELECT EmpID AS 'Employee ID', CONCAT(EmpLN," ",EmpMN," ",EmpFN) AS 'Full Name'
FROM Employees
;

/* using coalesce function */
SELECT EmpID AS 'Employee ID', CONCAT(EmpLN," ",COALESCE(EmpMN,'')," ",EmpFN) AS 'Full Name'
FROM Employees
;

/* using the concat_ws() function */
SELECT EmpID AS 'Employee ID', CONCAT_WS(" ",EmpLN,COALESCE(EmpMN,''),EmpFN) AS 'Full Name'
FROM Employees
;

/* change the DateIn to January 22, 2020 for 
the transaction id number 1 */
UPDATE Transactions
SET DateIN =  
WHERE TransID = 1 
;
/* change the date due to date out plus 5 days 
for the transId number 2 */


/* display the list of available DVDs for rent */
SELECT DVDID, DVDName, Status.StatDescrip
FROM DVDs INNER JOIN Status ON DVDs.StatID = Status.StatID
WHERE DVDs.StatID = 's2';

/* How many DVDs are available for rent? */
SELECT Status.StatDescrip,COUNT(DVDID) AS 'Number of available DVD'
FROM DVDs INNER JOIN Status ON DVDs.StatID = Status.StatID
WHERE DVDs.StatID = 's2';


/* 3. List all dvd id and names that have more than one disk. 
Sort disks in ascending order.
Flow control function
SYNTAX: if(condition, true_value, false_value) */
SELECT 
	D.DVDID AS 'DVD ID', 
	D.DVDName AS 'DVD Name',
    D.StatID AS 'Status',
    D.NumDisks AS 'Number of Disks',
    IF( (D.NumDisks > 1),'Check for Extra Disk!', 'Only One Disk') AS 'Message'
FROM DVDs as D
;
/* show the max rating of dvds */
SELECT
	COUNT(D.DVDID) AS `No. of DVDs Rated`,
    D.RatingID as 'Rating'
FROM DVDs as D
GROUP BY D.RatingID
ORDER BY `No. of DVDs Rated` DESC
LIMIT 3
;
