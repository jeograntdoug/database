/* Database I Final Exam Data - Winter 2020
Your Name: Donghyeok Seo

• THIS IS A CLOSE BOOK EXAM. 
• Save this file as Your_Name_DB_1_FE. 
• Submit only this script with all your answers.
• The Exam starts at 9:00 pm and ends at 12:05 pm.

Using NotePad++, Create the BookSales database that contains five tables: 
1) Authors, 
2) AuthorBook, 
3) Publishers, 
4) BookPublisher, and 
5) Books.

The Books table includes four columns: 
	• BookID, 
	• BookTitle, 
	• Copyright, and 
	• PubID. 
The primary key in the Books table is made up of the BookID column and no other columns.

The publisher listed in the Publishers table can publish one or more books, and that publisher is identified by the value in the foreign key column (PubID).

The AuthorBook table includes an FK1 and an FK2 because there are two foreign keys. 
The numbers are used because some tables might include multiple foreign keys in which one or more of those keys are made up of more than one column. The foreign keys in the AuthorBook table participate in one-to-many relationships with the Authors table and the Books table.

There are generally four steps that you should follow when developing a data model:
❑ Identifying the potential entities that will exist in the database
❑ Normalizing the data design to support data storage in the identified entities
❑ Identifying the relationships that exist between tables
❑ Refining the data model to ensure full normalization


The first step in developing a data model is to identify the objects (entities and attributes) that represent the type of data stored in your database. The purpose of this step is to name any types of information, categories, or actions that require the storage of data in the database. 
*/

/*
1. Create the BookSales_FirstName database that includes no optional components. */
CREATE DATABASE BookSales_FirstName;


/*switch to BookSales_FirstName database */
use BookSales_FirstName;

/*
2. Creating Tables */


/* ***** 1. create the Publishers table  ***** */
CREATE TABLE Publishers
(
	PubID SMALLINT NOT NULL PRIMARY KEY,
	PubName VARCHAR(40) NOT NULL,
	PubCity VARCHAR(20) NOT NULL,
	PubState CHAR(2) NOT NULL
)
;

/* Display the definition of the table Publishers */
DESCRIBE Publishers;


/* ***** 2. create the Authors table  ***** */

CREATE TABLE Authors
(
	AuthID SMALLINT NOT NULL PRIMARY KEY,
	AuthFN VARCHAR(20) NOT NULL,
	AuthMN VARCHAR(20) NULL,
	AuthLN VARCHAR(20) NOT NULL,
	Born YEAR NOT NULL,
	Died YEAR NULL
)
;

/* Set the default value for the column AuthMN to null */
ALTER TABLE Authors
	ALTER COLUMN AuthMN SET DEFAULT NULL
;

/* ***** 3. create the AuthorBook table  ***** */
CREATE TABLE AuthorBook
(
	AuthID SMALLINT NOT NULL,
	BookID SMALLINT NOT NULL,
	CONSTRAINT pk_AuthorBook PRIMARY KEY
	(
		AuthID asc,
		BookID asc
	)
)
;

/* Display the definition of the table AuthorBook */
DESCRIBE AuthorBook;

/* ***** 4. create the BookPublisher table  ***** */
CREATE TABLE BookPublisher
(
	PubID SMALLINT NOT NULL,
	BookID SMALLINT NOT NULL,
)
;

ALTER TABLE BookPublisher
	ADD CONSTRAINT pk_BookPublisher PRIMARY KEY (PubID,BookID)
;

/* ***** 5. create the Books table  ***** */
CREATE TABLE Books
(
	BookID SMALLINT NOT NULL PRIMARY KEY,
	BookTitle VARCHAR(60) NOT NULL,
	Copyright YEAR NOT NULL,
	PubID SMALLINT NOT NULL
)
;

/* delete the column PubID from the table Books */
ALTER TABLE Books
	DROP COLUMN PubID
;


-- Foreign key constraint(s)
-- 1. Between AuthorBook and Authors tables
ALTER TABLE AuthorBook
	ADD CONSTRAINT fk_AuthorBook_Authors FOREIGN KEY (AuthID)
	REFERENCES Authors (AuthID)
;

-- 2. Between AuthorBook and Books tables
ALTER TABLE AuthorBook
	ADD CONSTRAINT fk_AuthorBook_Books FOREIGN KEY (BookID)
	REFERENCES Books (BookID)
;

-- 3. Between Books and  BookPublisher tables
ALTER TABLE BookPublisher
	ADD CONSTRAINT fk_BookPublisher_Books FOREIGN KEY (BookID)
	REFERENCES Books (BookID)
;

-- 4. Between Publishers and  BookPublisher tables
ALTER TABLE BookPublisher
	ADD CONSTRAINT fk_BookPublisher_Publishers FOREIGN KEY (PubID)
	REFERENCES Publishers (PubID)
;

/*
3. Populate all tables with data through script data entry (using insert into/ values and Load Data Infile).
Data files are attached to the exam as csv files.
*/

-- load data from external .cvs file 

-- 1- Publishers table
LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Book_Sales_Data/Publishers.csv'
INTO TABLE Publishers
FIELDS TERMENATIED BY ','
LINES TERMENATIED BY '\r\n'
;

-- 2- Authors table
LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Book_Sales_Data/Authors.csv'
INTO TABLE Authors
FIELDS TERMENATIED BY ','
LINES TERMENATIED BY '\r\n'
;
-- 3- AuthorBook table
LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Book_Sales_Data/AuthorBook.csv'
INTO TABLE AuthorBook
FIELDS TERMENATIED BY ','
LINES TERMENATIED BY '\r\n'
;
-- 4- BookPublisher table
INSERT INTO BookPublisher
VALUES (
(736,2075),
(736,2091),
(736,2106),
(736,3333),
(736,7777),
(877,1372),
(877,2222),
(877,3021),
(877,3026),
(877,3218),
(877,4203),
(877,7778),
(1389,1032),
(1389,1035),
(1389,1111),
(1389,7832),
(1389,8888),
(1389,9999),
)
;
-- 5- Books table
LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Book_Sales_Data/Books.csv'
INTO TABLE Books
FIELDS TERMENATIED BY ','
LINES TERMENATIED BY '\r\n'
;
;

/*
4. Modify the definition of the following objects using scripts: */

/* 4.1. 
Publisher: add publisher country */
ALTER TABLE Publishers
	ADD COLUMN country VARCHAR(10) NOT NULL
;
/* 4.2. 
Author: author address (address, city, state, postal code, country) */
ALTER TABLE Authors
	ADD COLUMN address VARCHAR (30) NOT NULL,
	ADD COLUMN city VARCHAR (10) NOT NULL,
	ADD COLUMN state CHAR (2) NULL,
	ADD COLUMN postalcode VARCHAR (10) NOT NULL,
	ADD COLUMN country VARCHAR (15) NOT NULL,
;

/* 4.3. 
Modify the structure of the following objects:
Books: type (business, psychology , ...), price, notes
*/
ALTER TABLE Books
	ADD COLUMN type ENUM('business','psychology','mod_cook','Computers','trad_cook','UNDECIDED') NOT NULL,
	ADD COLUMN price DOUBLE NULL,
	ADD COLUMN notes VARCHAR(255) NULL
;

/* Add a unique value to Author ID (supposing that Author ID is char(5) */
ALTER TABLE Authors
	ALTER COLUMN AuthID SET UNIQUE
;

/* 5. Retrieve all author numbers (ID) and prevent duplicated values in the Author Book table */
SELECT A.AuthID , B.BookTitle
FROM Authors AS A 
	JOIN AuthorBook AS AB ON A.AuthID = AB.AuthID
	JOIN (SELECT DISTINCT BookTitle FROM Books) AS B ON AB.BookID = B.BookID
;	
	
/* 6. Find out how many authors published more than 5 books */
SELECT COUNT(AB.AuthID) AS `Count Authors more than 5 books`
FROM (
	SELECT AB.AuthID, COUNT(AB.BookID) AS `Published book`
	FROM AuthorBook AS AB
	GROUP BY AB.AuthID
	HAVING COUNT(AB.BookID) > 5
)

/* 7. Find out how many books were published by each publisher */
SELECT P.PubID, P.PubName, COUNT(BP.BookID) AS `Published Book`
FROM Publishers AS P JOIN BookPublisher AS BP ON P.PubID = BP.PubID
GROUP BY P.PubID
;

/* 8. Return author full name and full address */
SELECT 
	CONCAT_WS(' ',A.AuthLN,COALESCE(A.AuthMN,''),A.AuthFN) AS `Full Name`,
	CONCAT_WS(' ',A.address,A.city, COALESCE(A.state,''), A.postalcode, A.country)
FROM Authers AS A
;

/* 9. Find out author full name, book title and publisher name. */
SELECT 
	CONCAT_WS(' ',A.AuthLN,COALESCE(A.AuthMN,''),A.AuthFN) AS `Full Name`,
	B.BookTitle AS `Book Title`,
	P.PubName AS `Publisher Name`
FROM Authers AS A 
	JOIN AutherBook AS AB ON A.AuthID = AB.AuthID
	JOIN Books AS B ON AB.BookID = B.BookID
	JOIN BookPublisher AS BP ON AB.BookID = BP.BookID
	JOIN Publishers AS P ON BP.PubID = P.PubID
;

/* 10. Find out the number of book titles per type */
SELECT B.type, COUNT(B.BookTitle) AS `Number of book titles`
FROM Books
GROUP BY B.Type
ORDER BY B.Type ASC
;
