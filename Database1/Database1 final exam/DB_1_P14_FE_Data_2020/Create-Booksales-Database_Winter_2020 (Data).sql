/* Database I Final Exam Data - Winter 2020
Your Name: 

• THIS IS A CLOSE BOOK EXAM. 
• Save this file as Your_Name_DB_1_FE. 
• Submit only this script with all your answers.
• The Exam starts at 9:00 pm and ends at 12:05 pm.

Using NotePad++, Create the BookSales database that contains five tables: 
1) Authors, 
2) AuthorBook, 
3) Publishers, 
4) BookPublisher, and 
5) Books.

The Books table includes four columns: 
	• BookID, 
	• BookTitle, 
	• Copyright, and 
	• PubID. 
The primary key in the Books table is made up of the BookID column and no other columns.

The publisher listed in the Publishers table can publish one or more books, and that publisher is identified by the value in the foreign key column (PubID).

The AuthorBook table includes an FK1 and an FK2 because there are two foreign keys. 
The numbers are used because some tables might include multiple foreign keys in which one or more of those keys are made up of more than one column. The foreign keys in the AuthorBook table participate in one-to-many relationships with the Authors table and the Books table.

There are generally four steps that you should follow when developing a data model:
❑ Identifying the potential entities that will exist in the database
❑ Normalizing the data design to support data storage in the identified entities
❑ Identifying the relationships that exist between tables
❑ Refining the data model to ensure full normalization


The first step in developing a data model is to identify the objects (entities and attributes) that represent the type of data stored in your database. The purpose of this step is to name any types of information, categories, or actions that require the storage of data in the database. 
*/

/*
1. Create the BookSales_FirstName database that includes no optional components. */



/*switch to BookSales_FirstName database */


/*
2. Creating Tables */


/* ***** 1. create the Publishers table  ***** */


/* Display the definition of the table Publishers */


/* ***** 2. create the Authors table  ***** */

/* Set the default value for the column AuthMN to null */

/* ***** 3. create the AuthorBook table  ***** */

/* Display the definition of the table AuthorBook */

/* ***** 4. create the BookPublisher table  ***** */

/* ***** 5. create the Books table  ***** */

/* delete the column PubID from the table Books */



-- Foreign key constraint(s)
-- 1. Between AuthorBook and Authors tables


-- 2. Between AuthorBook and Books tables


-- 3. Between Books and  BookPublisher tables


-- 4. Between Books and  BookPublisher tables


-- 4. Between Publishers and  BookPublisher tables


/*
3. Populate all tables with data through script data entry (using insert into/ values and Load Data Infile).
Data files are attached to the exam as csv files.
*/

-- load data from external .cvs file 

-- 1- Publishers table

-- 2- Authors table

-- 3- AuthorBook table

-- 4- BookPublisher table

-- 5- Books table


/*
4. Modify the definition of the following objects using scripts: */

/* 4.1. 
Publisher: add publisher country */

/* 4.2. 
Author: author address (address, city, state, postal code, country) */


/* 4.3. 
Modify the structure of the following objects:
Books: type (business, psychology , ...), price, notes
*/



/* Add a unique value to Author ID (supposing that Author ID is char(5) */
	
/* 5. Retrieve all author numbers (ID) and prevent duplicated values in the Author Book table */

/* 6. Find out how many authors published more than 5 books */


/* 7. Find out how many books were published by each publisher */


/* 8. Return author full name and full address */


/* 9. Find out author full name, book title and publisher name. */


/* 10. Find out the number of book titles per type */

