
/* Constraint type:
1. primary key --> pk_table_name (pk_Customers)
2. foreign key --> fk_table_name1_table_name2 (fk_Orders_Customers)
3. unique --> uq_column_name_table_name (uq_CompanyName_Suppliers)
4. default --> df_column_name_table_name (df_City_Customers)
5. check -->ck_column_name1_column_name2 (ck_OrderDate_ShippedDate)
*/

/* add a primary key to an existing table 
alter table table_name
	add constraint pk_table_name primary key [clustered] (column_name [asc])
*/

/* check constraint
Syntax:
	alter table table_name
		add constraint ck_column_name_table_name check (condition)
*/

use hflix2020;

-- Set DVDID AUTO_INCREMENT table DVDs

ALTER TABLE DVDs
	MODIFY COLUMN DVDID SMALLINT AUTO_INCREMENT
;

ALTER TABLE DVDs
	ALTER COLUMN NumDisks SET DEFAULT 1
    -- or MODIFY COLUMN NumDisks TINYINT DEFAULT 1
;

-- ADD Primary key in the table MovieTypes
ALTER TABLE MovieTypes
	ADD CONSTRAINT PRIMARY KEY clustered (MTypeID)
;

-- Nullable table Customers
ALTER TABLE Customers
	MODIFY COLUMN CustMN varchar(10) NULL;
;

-- Change EmpMN langth to 10 table employees
ALTER TABLE Employees
	MODIFY COLUMN EmpMN varchar(10) NULL
;

-- Change column length of PartMN to 10 and Nullable table Participants
ALTER TABLE Participants
	MODIFY COLUMN PartMN VARCHAR(10) NULL
;

-- DateIN is Nullable table Transactions
ALTER TABLE Transactions
	MODIFY COLUMN DateIN DATE NULL
;

DESCRIBE Customers;


DESCRIBE DVDParticipants;

-- --------------- Building Relationship----------------------
-- Foreign key TABLE DVDParticipants
ALTER TABLE DVDParticipants
	ADD CONSTRAINT fk_DVDParticipants_DVDs FOREIGN KEY (DVDID)
    REFERENCES DVDs (DVDID)
;

ALTER TABLE DVDParticipants
	ADD CONSTRAINT fk_DVDParticipants_Participants FOREIGN KEY (PartID)
	REFERENCES Participants (PartID)
;

ALTER TABLE DVDParticipants
	ADD CONSTRAINT fk_DVDParticipants_Roles FOREIGN KEY (RoleID)
    REFERENCES Roles (RoleID)
;

-- Foreign key Table DVDs
ALTER TABLE DVDs
	ADD CONSTRAINT fk_DVDs_MovieTypes FOREIGN KEY (MTypeID)
    REFERENCES MovieTypes (MTypeID)
;

ALTER TABLE DVDs
	ADD CONSTRAINT fk_DVDs_Studios FOREIGN KEY (StudID)
    REFERENCES Studios (StudID)
;

ALTER TABLE DVDs
	ADD CONSTRAINT fk_DVDs_Ratings FOREIGN KEY (RatingID)
    REFERENCES Ratings (RatingID)
;

ALTER TABLE DVDs
	ADD CONSTRAINT pk_DVDs_Formats FOREIGN KEY (FormID)
    REFERENCES Formats (FormID)
;

ALTER TABLE DVDs
	ADD CONSTRAINT pk_DVDs_StatID FOREIGN KEY (StatID)
    REFERENCES Status (StatID)
;

ALTER TABLE DVDs
	DROP FOREIGN KEY pk_DVDs_Formats
;

ALTER TABLE DVDs
	DROP FOREIGN KEY pk_DVDs_StatID
;

ALTER TABLE DVDs
	ADD CONSTRAINT fk_DVDs_Formats FOREIGN KEY (FormID)
    REFERENCES Formats (FormID)
;

ALTER TABLE DVDs
	ADD CONSTRAINT fk_DVDs_Status FOREIGN KEY (StatID)
    REFERENCES Status (StatID)
;


-- Foreign key table Orders
ALTER TABLE Orders
	ADD CONSTRAINT fk_Orders_Customers FOREIGN KEY (CustID)
    REFERENCES Customers (CustID)
;

ALTER TABLE Orders
	ADD CONSTRAINT fk_Orders_Employees FOREIGN KEY (EmpID)
    REFERENCES Employees (EmpID)
;

-- Foreign key table Transactions
ALTER TABLE Transactions
	ADD CONSTRAINT fk_Transactions_Orders FOREIGN KEY (OrderID)
    REFERENCES Orders (OrderID)
;

ALTER TABLE Transactions
	ADD CONSTRAINT fk_Transactions_DVDID FOREIGN KEY (DVDID)
    REFERENCES DVDs (DVDID)
;

-- CHECK / DateDue >= DateOut
ALTER TABLE Transactions
	ADD CONSTRAINT ck_DataDue_DateOut_Transactions
    CHECK (DateDue >= DateOut)
;

SELECT *
FROM information_schema.table_constraints
;
